################################################################################
# Automatically-generated file. Do not edit!
################################################################################

C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
OBJ_SRCS := 
CC_SRCS := 
ASM_SRCS := 
C_SRCS := 
CPP_SRCS := 
O_SRCS := 
S_UPPER_SRCS := 
CC_DEPS := 
C++_DEPS := 
EXECUTABLES := 
OBJS := 
C_UPPER_DEPS := 
CXX_DEPS := 
C_DEPS := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
src \
src/DVSAlgorithms \
src/FEMImplementado/Funcionales \
src/FEMImplementado/Funciones/Bases \
src/FEMImplementado/Funciones \
src/FEMImplementado/Funciones/LadDer/Elasticidad \
src/FEMImplementado/Funciones/LadDer \
src/FEMImplementado/Funciones/PorTramos \
src/FEMImplementado/Funciones/Solutions \
src/FEMImplementado/MetNumBasicos \
src/FEMImplementado/MetSolLoc \
src/FEMImplementado/Omega \
src/Geometry/Mesh \
src/Geometry \
src/MtxVectOperations \
src/NumericalMethods \
src/ParallelApplication \
src/Parameters \
src/SchurComplement \

