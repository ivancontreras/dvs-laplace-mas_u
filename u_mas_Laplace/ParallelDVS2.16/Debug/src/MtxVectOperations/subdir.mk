################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/MtxVectOperations/Local_Inverse.cpp \
../src/MtxVectOperations/OpMatxVect.cpp \
../src/MtxVectOperations/SparseMatx.cpp 

OBJS += \
./src/MtxVectOperations/Local_Inverse.o \
./src/MtxVectOperations/OpMatxVect.o \
./src/MtxVectOperations/SparseMatx.o 

CPP_DEPS += \
./src/MtxVectOperations/Local_Inverse.d \
./src/MtxVectOperations/OpMatxVect.d \
./src/MtxVectOperations/SparseMatx.d 


# Each subdirectory must supply rules for building sources it contributes
src/MtxVectOperations/%.o: ../src/MtxVectOperations/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


