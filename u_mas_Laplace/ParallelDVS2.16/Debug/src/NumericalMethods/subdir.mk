################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/NumericalMethods/BandSolve.cpp \
../src/NumericalMethods/CGM.cpp 

OBJS += \
./src/NumericalMethods/BandSolve.o \
./src/NumericalMethods/CGM.o 

CPP_DEPS += \
./src/NumericalMethods/BandSolve.d \
./src/NumericalMethods/CGM.d 


# Each subdirectory must supply rules for building sources it contributes
src/NumericalMethods/%.o: ../src/NumericalMethods/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


