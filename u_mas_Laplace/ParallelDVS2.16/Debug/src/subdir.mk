################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/AlgorithmDVS.cpp \
../src/BandSolve.cpp \
../src/BilinealGrad.cpp \
../src/CGM.cpp \
../src/CombinaCoef.cpp \
../src/ConversionBinaria.cpp \
../src/ConversionTrinaria.cpp \
../src/Cuadratura.cpp \
../src/DVSSchurComplement.cpp \
../src/DVS_BDDC.cpp \
../src/DVS_S_inv.cpp \
../src/ErrorControl.cpp \
../src/FDMCoef.cpp \
../src/FDMLaplace.cpp \
../src/Forma_Bilineal.cpp \
../src/FunV_Elast.cpp \
../src/Func_Base.cpp \
../src/Func_Tramo.cpp \
../src/Funcion.cpp \
../src/Funcional_rhs.cpp \
../src/GeneraGeom.cpp \
../src/Lineal.cpp \
../src/Local_Inverse.cpp \
../src/Mesh.cpp \
../src/MetSolLocal.cpp \
../src/NodoG.cpp \
../src/OpBilElast.cpp \
../src/OpInterface.cpp \
../src/OpMatxVect.cpp \
../src/ParallelDVS2.16.cpp \
../src/ParallelScheme.cpp \
../src/PropDef.cpp \
../src/Properties.cpp \
../src/SchurComplement.cpp \
../src/SinPixSinPiySinPiz.cpp \
../src/SparseMatx.cpp \
../src/SubDom.cpp \
../src/f_2D.cpp \
../src/f_3D.cpp \
../src/f_om_elast1.cpp \
../src/f_om_elast2.cpp \
../src/f_om_elast3.cpp 

O_SRCS += \
../src/AlgorithmDVS.o \
../src/BandSolve.o \
../src/BilinealGrad.o \
../src/CGM.o \
../src/CombinaCoef.o \
../src/ConversionBinaria.o \
../src/ConversionTrinaria.o \
../src/Cuadratura.o \
../src/DVSSchurComplement.o \
../src/DVS_BDDC.o \
../src/DVS_S_inv.o \
../src/ErrorControl.o \
../src/FDMCoef.o \
../src/FDMLaplace.o \
../src/Forma_Bilineal.o \
../src/FunV_Elast.o \
../src/Func_Base.o \
../src/Func_Tramo.o \
../src/Funcion.o \
../src/Funcional_rhs.o \
../src/GeneraGeom.o \
../src/Lineal.o \
../src/Local_Inverse.o \
../src/Mesh.o \
../src/MetSolLocal.o \
../src/NodoG.o \
../src/OpBilElast.o \
../src/OpInterface.o \
../src/OpMatxVect.o \
../src/ParallelDVS2.16.o \
../src/ParallelScheme.o \
../src/PropDef.o \
../src/Properties.o \
../src/SchurComplement.o \
../src/SinPixSinPiySinPiz.o \
../src/SparseMatx.o \
../src/SubDom.o \
../src/f_2D.o \
../src/f_3D.o \
../src/f_om_elast1.o \
../src/f_om_elast2.o \
../src/f_om_elast3.o 

OBJS += \
./src/AlgorithmDVS.o \
./src/BandSolve.o \
./src/BilinealGrad.o \
./src/CGM.o \
./src/CombinaCoef.o \
./src/ConversionBinaria.o \
./src/ConversionTrinaria.o \
./src/Cuadratura.o \
./src/DVSSchurComplement.o \
./src/DVS_BDDC.o \
./src/DVS_S_inv.o \
./src/ErrorControl.o \
./src/FDMCoef.o \
./src/FDMLaplace.o \
./src/Forma_Bilineal.o \
./src/FunV_Elast.o \
./src/Func_Base.o \
./src/Func_Tramo.o \
./src/Funcion.o \
./src/Funcional_rhs.o \
./src/GeneraGeom.o \
./src/Lineal.o \
./src/Local_Inverse.o \
./src/Mesh.o \
./src/MetSolLocal.o \
./src/NodoG.o \
./src/OpBilElast.o \
./src/OpInterface.o \
./src/OpMatxVect.o \
./src/ParallelDVS2.16.o \
./src/ParallelScheme.o \
./src/PropDef.o \
./src/Properties.o \
./src/SchurComplement.o \
./src/SinPixSinPiySinPiz.o \
./src/SparseMatx.o \
./src/SubDom.o \
./src/f_2D.o \
./src/f_3D.o \
./src/f_om_elast1.o \
./src/f_om_elast2.o \
./src/f_om_elast3.o 

CPP_DEPS += \
./src/AlgorithmDVS.d \
./src/BandSolve.d \
./src/BilinealGrad.d \
./src/CGM.d \
./src/CombinaCoef.d \
./src/ConversionBinaria.d \
./src/ConversionTrinaria.d \
./src/Cuadratura.d \
./src/DVSSchurComplement.d \
./src/DVS_BDDC.d \
./src/DVS_S_inv.d \
./src/ErrorControl.d \
./src/FDMCoef.d \
./src/FDMLaplace.d \
./src/Forma_Bilineal.d \
./src/FunV_Elast.d \
./src/Func_Base.d \
./src/Func_Tramo.d \
./src/Funcion.d \
./src/Funcional_rhs.d \
./src/GeneraGeom.d \
./src/Lineal.d \
./src/Local_Inverse.d \
./src/Mesh.d \
./src/MetSolLocal.d \
./src/NodoG.d \
./src/OpBilElast.d \
./src/OpInterface.d \
./src/OpMatxVect.d \
./src/ParallelDVS2.16.d \
./src/ParallelScheme.d \
./src/PropDef.d \
./src/Properties.d \
./src/SchurComplement.d \
./src/SinPixSinPiySinPiz.d \
./src/SparseMatx.d \
./src/SubDom.d \
./src/f_2D.d \
./src/f_3D.d \
./src/f_om_elast1.d \
./src/f_om_elast2.d \
./src/f_om_elast3.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


