################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Parameters/ErrorControl.cpp \
../src/Parameters/PropDef.cpp \
../src/Parameters/Properties.cpp 

OBJS += \
./src/Parameters/ErrorControl.o \
./src/Parameters/PropDef.o \
./src/Parameters/Properties.o 

CPP_DEPS += \
./src/Parameters/ErrorControl.d \
./src/Parameters/PropDef.d \
./src/Parameters/Properties.d 


# Each subdirectory must supply rules for building sources it contributes
src/Parameters/%.o: ../src/Parameters/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


