################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DVSAlgorithms/AlgorithmDVS.cpp \
../src/DVSAlgorithms/DVSSchurComplement.cpp \
../src/DVSAlgorithms/DVS_BDDC.cpp \
../src/DVSAlgorithms/DVS_S_inv.cpp 

OBJS += \
./src/DVSAlgorithms/AlgorithmDVS.o \
./src/DVSAlgorithms/DVSSchurComplement.o \
./src/DVSAlgorithms/DVS_BDDC.o \
./src/DVSAlgorithms/DVS_S_inv.o 

CPP_DEPS += \
./src/DVSAlgorithms/AlgorithmDVS.d \
./src/DVSAlgorithms/DVSSchurComplement.d \
./src/DVSAlgorithms/DVS_BDDC.d \
./src/DVSAlgorithms/DVS_S_inv.d 


# Each subdirectory must supply rules for building sources it contributes
src/DVSAlgorithms/%.o: ../src/DVSAlgorithms/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


