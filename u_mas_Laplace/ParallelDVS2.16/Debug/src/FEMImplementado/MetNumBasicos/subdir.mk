################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/FEMImplementado/MetNumBasicos/CombinaCoef.cpp \
../src/FEMImplementado/MetNumBasicos/ConversionBinaria.cpp \
../src/FEMImplementado/MetNumBasicos/ConversionTrinaria.cpp \
../src/FEMImplementado/MetNumBasicos/Cuadratura.cpp 

OBJS += \
./src/FEMImplementado/MetNumBasicos/CombinaCoef.o \
./src/FEMImplementado/MetNumBasicos/ConversionBinaria.o \
./src/FEMImplementado/MetNumBasicos/ConversionTrinaria.o \
./src/FEMImplementado/MetNumBasicos/Cuadratura.o 

CPP_DEPS += \
./src/FEMImplementado/MetNumBasicos/CombinaCoef.d \
./src/FEMImplementado/MetNumBasicos/ConversionBinaria.d \
./src/FEMImplementado/MetNumBasicos/ConversionTrinaria.d \
./src/FEMImplementado/MetNumBasicos/Cuadratura.d 


# Each subdirectory must supply rules for building sources it contributes
src/FEMImplementado/MetNumBasicos/%.o: ../src/FEMImplementado/MetNumBasicos/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


