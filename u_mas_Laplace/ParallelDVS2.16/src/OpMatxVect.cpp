/*
 * OpMatxVect.cpp
 *
 *  Created on: 18/03/2014
 *      Author: argonauta
 */

#include "OpMatxVect.hpp"

OpMatxVect::OpMatxVect(int dim, int stencil, int *ntp) {
	// TODO Auto-generated constructor stub
	nDVect= 1;
	support = stencil;
	ntype = ntp;

}

void OpMatxVect::MatxVectValues(double ****A, double **b){

	coef = A;
	rhs = b;
}


void OpMatxVect::applyMtxVect(double *w,double *v,int *mA, int *mV){


	int i,j;


    for (i = 0; i < num_nodes; i++)
	 {
       if(mA[i] < 0)
		 continue;

		for(int k=0;k<nDVect;k++)
		  w[mA[i]*nDVect + k] = 0;


		for(int num_nodo=0;num_nodo< support ; num_nodo++)
		  {

		    j = nodo_sub[i][num_nodo];


		   // cout << "Por aquí pasó Colón con su batallón " << j << " "<< mV[j] << endl;

			    if ( j>=0 && mV[j] >= 0 )
			    {



			    for(int k=0;k<nDVect;k++)
		  	      for(int l=0;l<nDVect;l++)
		  	      {
		  	       w[mA[i]*nDVect + k] += (coef[i][num_nodo][k][l])*v[mV[j]*nDVect+l];
		  	      // cout << "  "  << coef[i][num_nodo][k][l] << " " << v[mV[j]*nDVect+l] << " "<< w[mA[i]*nDVect + k] << endl;
		  	      }
			    }

		      }

		  }
}

double calc_norm_square(double *w){




	return 0.;

}

void OpMatxVect::difference_vectors(double *v,double *w,int tam){

	int i,j;

	 /*for (i = 0; i < num_nodes; i++)
	  if ((j = mA[i]) >= 0)
	   for(int k=0;k<nDVect;k++)
		 	w[j*nDVect + k ] = v[j*nDVect + k] - w[j*nDVect + k];*/

	 for (i = 0; i < tam*nDVect; i++)
			 	w[i] = v[i] - w[i];

}

void OpMatxVect::inverse_A_NN(int *mV, int dV){

   int i,j,k,in_i,in_j,p,q;
   double val_insertar;

	A_NN = new SparseMatx(dV*nDVect,dV*nDVect,support*nDVect);

	for (k = 0; k < num_nodes; k++)
	 {
	  if ((i = mV[k]) < 0) continue;

	  for(int nodo_soporte=0; nodo_soporte< support ; nodo_soporte++)
	   {

		if (nodo_sub[k][nodo_soporte] < 0 ) continue;

	    j =  mV[nodo_sub[k][nodo_soporte]];

	    if (j >= 0 )//&& nodo_sub[k][nodo_soporte] >=0
	     {
	      for(p=0;p<nDVect;p++)
	       {
	        in_i= i*nDVect + p;

	        for( q=0;q<nDVect;q++)
	      	 {
	      	  in_j= j*nDVect + q;
	      	  val_insertar = coef[k][nodo_soporte][p][q];
	      	  A_NN->alloc(in_i,in_j,val_insertar);
	      	    	  }
                    }
	      	      }
	            }
	          }

	 solve = new BandSolve(dV*nDVect, A_NN);
	 delete A_NN;

}


void OpMatxVect::inverse(double *v,double *w)
{
   /* Computes scr[sc2][] = A(sp)-1*scr[sc1][].
    Erases other elements from scr[sc2][]  */
	 int i, j;

	 double *X;
	 double *Y;

	 X = new double[dimV*nDVect];

     for (i = 0; i < dimV*nDVect; i++)
    	 X[i] = 0.0;

	 Y = new double[dimV*nDVect];

	 for (i = 0; i < dimV*nDVect; i++)
		 Y[i] = 0.0;

	 for (i = 0; i < dimV*nDVect; i++)
		  Y[i] =w[i];

	 solve->solve(X, Y);

	 for (i = 0; i < dimV*nDVect; i++)
		  v[i] = X[i] ;

	  delete []X;
	  delete []Y;

}

double calc_norm_error();

void OpMatxVect::recover_rhs(double *v, int *mA,double *v1, int *mapS){

	int i,j;

	 for (i = 0; i < num_nodes; i++)
	  {
	    if ((j = mA[i]) >= 0)
	     for(int k=0;k<nDVect;k++)
		   v[j*nDVect + k ] = v1[mapS[i]*nDVect + k];

	  }

}


void OpMatxVect::recover_rhs(double *v, int *mA,double *v1){

	int i,j;

	 for (i = 0; i < num_nodes; i++)
	  {
	    if ((j = mA[i]) >= 0)
	     for(int k=0;k<nDVect;k++)
		   v[j*nDVect + k ] = v1[i*nDVect + k];
	  }

}

void OpMatxVect::recover_rhs(double *v, int *mA){

	int i,j;

	 for (i = 0; i < num_nodes; i++)
	  {
		if ((ntype[i] & KNOWN) != 0) continue;

	    if ((j = mA[i]) >= 0)
	     for(int k=0;k<nDVect;k++)
	   	   v[j*nDVect + k ] = rhs[i][k];
	  }

}


void OpMatxVect::set_param_apply(int n_nodes,int *mA,int numA, int *mV,int numV,int **n_sub){

	dimA = numA;
	dimV = numV;
	mapA = mA;
	mapV = mV;
	num_nodes = n_nodes;
	nodo_sub = n_sub;

}

OpMatxVect::~OpMatxVect() {
	// TODO Auto-generated destructor stub
	delete solve;
}
