/*
 * Local_Inverse.h
 *
 *  Created on: 24/08/2014
 *      Author: argonauta
 */

#ifndef LOCAL_INVERSE_H_
#define LOCAL_INVERSE_H_

#include "IterativeMethods.hpp"
#include "OpMatxVect.hpp"

class Local_Inverse: public IterativeMethods {
public:
	int *map_;
	int tam;
	OpMatxVect *OpMtxV;
	Local_Inverse(OpMatxVect *Op_Mv);
	void ApplyOp(double *x, double *y);
	double dot(double *x, double *y);
	void inverse_A_NN(int *mV, int dV);
	void inverse(double *v,double *w);
	int getSize(void);
	virtual ~Local_Inverse();
};

#endif /* LOCAL_INVERSE_H_ */
