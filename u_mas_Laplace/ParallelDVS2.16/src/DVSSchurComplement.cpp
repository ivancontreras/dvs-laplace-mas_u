/*
 * DVSSchurComplement.cpp
 *
 *  Created on: 31/03/2014
 *      Author: argonauta
 */

#include "DVSSchurComplement.hpp"
#include "CGM.hpp"
#include "IterativeMethods.hpp"

DVSSchurComplement::DVSSchurComplement(OpInterface *_op) : AlgorithmDVS(_op){
	// TODO Auto-generated constructor stub

}

void DVSSchurComplement::ApplyOp(double *x, double *y){

	//int face,n_p;
	Apply_S(x,y);
	//Apply operator a
	Op->calc_a(x);

/*	 if(Op->P->rank == 13)
		 face = 0;
	 if (Op->P->rank == 12)
		 face = 1;

	 if(Op->P->rank == 13 || Op->P->rank == 12)
	 	 {
	 	 for(int i=0; i < Op->Sub->k_[face];i++)
	 	 	{
	 	 	  n_p =  Op->Sub->node_face[face][i]; // number of node in fine mesh.

	 	 	  if(  (Op->Sub->ntype[n_p] & Op->Sub->DUAL)!= 0)
	 	 	  {
	 	 		  for(int j=0;j<Op->Sub->nDim;j++)
	 	 			  cout << Op->Sub->node_dual[n_p] << " " << j << " " << x[Op->Sub->node_dual[n_p]*Op->Sub->nDVect+j] << endl;

	 	 	  }
	 	 	}

	 	 cout << face<<  " "<<" "<<  Op->P->rank << " "<< Op->P->nbrs[face] << "************Comprueba**************************" << endl;

	 	 }*/
}

int DVSSchurComplement::getSize(void){

	int val_ret = 0;

	val_ret = SchurComp->dimA;

	return val_ret;
}

double DVSSchurComplement::dot(double *x, double *y){

 double dot_prod;
 double ret_dot;

 ret_dot = 0.;

 dot_prod = SchurComp->dot(x,y);


 ret_dot =  Op->call_dot(dot_prod);

 //cout<< Op->Sub->id_sub << " " << dot_prod <<  " "<< ret_dot <<  endl;

 return ret_dot;
}

void DVSSchurComplement::rhs(){


   SchurComp->rhs_SC(0,0,rhss);

   /*if(Op->Sub->id_sub == 11)
     for (int i = 0; i < Op->Sub->n_nodes; i++)
       if(Op->Sub->mapDual[i]>=0)
    	  for(int k=0;k<SchurComp->nDVect;k++)
    		  cout << "Dual "  << i << " " << Op->Sub->mapDual[i] << " " << rhss[ Op->Sub->mapDual[i]*SchurComp->nDVect + k ] << endl;*/


}

void DVSSchurComplement::solve(){
int face,n_p;
double *u_int;
double norm_err;
double norm_err_gl;

	//Define subsets of nodes to be used in the Schur complement.
	SchurComp->set_param_apply(Op->Sub->n_nodes,Op->Sub->mapDual,Op->Sub->nDual,Op->Sub->mapIntPri,Op->Sub->nIntPri,Op->Sub->Fem->FEM->nodo_sub);
	SchurComp->set_parameters_schur_int(Op->Sub->mapPrimal,Op->Sub->nPrim,Op->Sub->mapInt,Op->Sub->nInt);

	//Start arrays to be used in the iterative process.
    rhss = new double[SchurComp->dimA*SchurComp->nDVect];
    u = new double[SchurComp->dimA*SchurComp->nDVect];
    u_int = new double[SchurComp->dimA*SchurComp->nDVect];

    for (int i = 0; i < SchurComp->dimA*SchurComp->nDVect; i++)
	  u[i] = u_int[i] = rhss[i] = 0.0;

	 //Check A_pipi

	 //SchurComp->apply_A_pipi();

     // Build right hand side for this Schur Complement.
	 rhs();

	for (int i = 0; i < SchurComp->dimA*SchurComp->nDVect; i++)
		 u_int[i] = rhss[i];

	 IterativeMethods *pt = (IterativeMethods *) this;

	 CG = new CGM(*pt, 1e-9);

     CG->rank = Op->P->rank;
	 CG->imprime = true;

	// Apply_S(u,rhss);

	 //Op->calc_a(u);

	 //Apply_S_inv(rhss,u);

	// Op->calc_a(rhss);

	 CG->solve(u, rhss);

/*	 //if(Op->Sub->id_sub == 13)
	   for (int i = 0; i < Op->Sub->n_nodes; i++)
		 if(Op->Sub->mapDual[i]>=0)
		   for(int k=0;k<SchurComp->nDVect;k++)
		      cout << "Dual "  << i << " " << Op->Sub->mapDual[i] << " " << u[ Op->Sub->mapDual[i]*SchurComp->nDVect + k ] << endl;
*/

	 //if(Op->Sub->id_sub == 13)
	 /*  for(int i = 0; i < SchurComp->dimA*SchurComp->nDVect ; i++)
	     cout << u[i] << std::endl; //<<  " " << rhss[i]<< endl;*/
		Op->Sub->print_vector(Op->Sub->DUAL,u);
	//norm_err = calc_err(u);
    //norm_err_gl =  Op->call_dot(norm_err);



	 //after finishing iterative procedure make substitution
}


DVSSchurComplement::~DVSSchurComplement() {
	// TODO Auto-generated destructor stub
}

