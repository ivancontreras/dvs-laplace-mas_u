#ifndef __Solvable__
#define __Solvable__

#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <math.h>
using namespace std;

class Solvable
{

protected:

   const char *name;


public:

   bool imprime;
   int n;
   int rank;

   Solvable(void)
   {
      name = 0;
      imprime = false;
   }

   virtual ~Solvable(void)
   {
   }

   virtual void clean(void)=0;

   virtual void solve(double *x, double *y)=0;

   virtual int getIter(void)=0;

   const char *getName(void)
   {
      return name;
   }

};

#endif
