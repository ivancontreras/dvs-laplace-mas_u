/*
 * ParallelScheme.cpp
 *
 *  Created on: 27/02/2014
 *      Author: Iv�n
 */

#include "ParallelScheme.hpp"
#include "PropDef.hpp"

ParallelScheme::ParallelScheme() {
	// TODO Auto-generated constructor stub

}

ParallelScheme::ParallelScheme(int argc, char *argv[]) {
	// TODO Auto-generated constructor stub
	start_ParallelScheme(argc,argv);
}


void ParallelScheme::start_ParallelScheme(int argc, char *argv[]){

	int d;

	PropDef *props = new PropDef();
	props->parse(argc, argv);

	d = props->getInt("d", 0);

	int dims[2]={props->getInt("Nx"),props->getInt("Ny")};//,props->getInt("Nz")},
	int periods[2]={0,0}, reorder=0;

    inbuf = new int[d*2];

    for(int i=0;i<d*2;i++)
	inbuf[i] = MPI_PROC_NULL;

    tag=1;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

	if (numtasks == props->getInt("np")) {
	  MPI_Cart_create(MPI_COMM_WORLD, d, dims, periods, reorder, &cartcomm);
	  MPI_Comm_rank(cartcomm, &rank);
	  MPI_Cart_coords(cartcomm, rank, d, coords);
	  MPI_Cart_shift(cartcomm, 0, 1, &nbrs[DOWN], &nbrs[UP]);
      MPI_Cart_shift(cartcomm, 1, 1, &nbrs[LEFT], &nbrs[RIGHT]);

      if(d>2)
	    MPI_Cart_shift(cartcomm, 2, 1, &nbrs[BACK], &nbrs[FRONT]);
	  //exchange_ParallelScheme();

	}
	else
		printf("Must specify %d processors. Terminating.\n",props->getInt("np"));

}

void ParallelScheme::stop_ParallelScheme(){

	MPI_Finalize();

	printf("\n\nEnd Program\n\n");
}

ParallelScheme::~ParallelScheme() {
	// TODO Auto-generated destructor stub
}
