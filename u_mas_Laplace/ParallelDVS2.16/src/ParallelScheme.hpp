/*
 * ParallelScheme.h
 *
 *  Created on: 27/02/2014
 *      Author: Iv�n
 */
#include "mpi.h"
#include <iostream>
using namespace std;
#ifndef PARALLELSCHEME_H_
#define PARALLELSCHEME_H_
#define SIZE 27

#define LEFT   0
#define RIGHT  1
#define DOWN   2
#define UP     3
#define BACK   4
#define FRONT  5

class ParallelScheme {

public:
	MPI_Comm cartcomm;
	MPI_Request reqs[2];
	MPI_Status stats[2];
    int rank;
    int *inbuf;
    int *arr_source;
    int *arr_dest;
    int nbrs[4];
    int coords[3];
    int numtasks, source, dest, outbuf, i, tag;
	ParallelScheme();
	ParallelScheme(int argc, char *argv[]);
	void barrera_();
	void start_ParallelScheme(int argc, char *argv[]);
	void stop_ParallelScheme();
	~ParallelScheme();

};

#endif /* PARALLELSCHEME_H_ */
