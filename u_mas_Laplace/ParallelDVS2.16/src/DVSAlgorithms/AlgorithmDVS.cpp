/*
 * AlgorithmDVS.cpp
 *
 *  Created on: 31/03/2014
 *      Author: argonauta
 */

#include "AlgorithmDVS.hpp"

AlgorithmDVS::AlgorithmDVS(OpInterface *_op) {
	// TODO Auto-generated constructor stub
  int _d;

  Op = _op;
  //_d = pow(3,Op->Sub->nDim);
  _d = Op->Sub->Fem->FEM->tam_arreglo;
  SchurComp = new SchurComplement(_op,_op->Sub->nDim,_d,_op->Sub->ntype,false);
  SchurComp->MatxVectValues(_op->Sub->Fem->FEM->coef, _op->Sub->Fem->FEM->rhs);
  SchurComp->set_param_apply(Op->Sub->n_nodes,Op->Sub->mapDual,Op->Sub->nDual,Op->Sub->mapIntPri,Op->Sub->nIntPri,Op->Sub->Fem->FEM->nodo_sub);

  vv = new double[Op->Sub->nBdPrim*Op->Sub->nDVect] ;
  ww = new double[Op->Sub->nBdPrim*Op->Sub->nDVect] ;
}

void AlgorithmDVS::apply_j(double *x){

	double *yy;

	yy = new double[Op->Sub->nDual*Op->Sub->nDim] ;

	for(int i=0;i< Op->Sub->nDual*Op->Sub->nDim;i++)
		yy[i] = x[i];

	Op->calc_a(x);

	for(int i=0;i< Op->Sub->nDual*Op->Sub->nDim;i++)
		x[i] = yy[i] - x[i];

	delete []yy;



}

double AlgorithmDVS::calc_err(double *u_aprox){

	double *coor_;
    double *err_;
    double error_;
    double val_ret;
	int m,m1,j,i;

	coor_ = new double[Op->Sub->nDim];
	err_= new double[Op->Sub->nDual*Op->Sub->nDim];

	SinPixSinPiySinPiz *exacta = new SinPixSinPiySinPiz();

    error_ = 0.0;
    val_ret = 0.0;
   //calcula soluci�n exacta anal�tica
   //obt�n la diferencia r = abs(analitica - aprox)

   //multiplica por A por la diferencia y almac�nalo en e
   //

    for (i = 0; i < Op->Sub->n_nodes; i++)
      {
    	Op->Sub->getCoord(i,coor_);

       if(Op->Sub->mapDual[i]>=0)

       for(int k=0;k<Op->Sub->nDVect;k++)
       {
         err_[i*Op->Sub->nDVect + k] = fabs(u_aprox[Op->Sub->mapDual[i]*Op->Sub->nDVect + k] - exacta->evalua(coor_));
         error_+= err_[i*Op->Sub->nDVect + k] * err_[i*Op->Sub->nDVect + k];

         }

       }


	return error_;
}

void AlgorithmDVS::Apply_S(double *v, double *w){


	//Define subsets of nodes to be used in the Schur complement.
  SchurComp->mapA = Op->Sub->mapDual;
  SchurComp->mapV = Op->Sub->mapIntPri;
  SchurComp->set_parameters_schur_int(Op->Sub->mapPrimal,Op->Sub->nPrim,Op->Sub->mapInt,Op->Sub->nInt);
  SchurComp->ApplyOp(v,w);
}


void AlgorithmDVS::Apply_S_inv(double *v, double *w){

	//Define subsets of nodes to be used in the Schur complement.
	SchurComp->set_parameters_schur_int(Op->Sub->mapPrimal,Op->Sub->nPrim,Op->Sub->mapFull,Op->Sub->nFull);
	SchurComp->mapV = Op->Sub->mapFullPri;

	//prepare values to map the results to dual nodes values.



	for (int i = 0; i < Op->Sub->nBdPrim*Op->Sub->nDVect; i++)
		ww[i] = 0.;

	//We  set up Interior equal to zero.

	for (int i = 0; i < SchurComp->num_nodes; i++)
	 if(Op->Sub->mapDual[i]>=0)
	  for(int k=0;k<Op->Sub->nDVect;k++)
	    ww[ Op->Sub->mapFullPri[i]*Op->Sub->nDVect + k ] = w[Op->Sub->mapDual[i]*Op->Sub->nDVect + k];


	//Apply internal Schur Complement
	SchurComp->apply_internal_SC(vv,ww); // define the right parameters for this method in this class.




	//Recover values for Delta to continue

	for (int i = 0; i < SchurComp->num_nodes; i++)
	 if(Op->Sub->mapFullPri[i]>=0 && Op->Sub->mapDual[i]>=0)
	  for(int k=0;k<Op->Sub->nDVect;k++)
	    v[ Op->Sub->mapDual[i]*Op->Sub->nDVect + k ] = vv[Op->Sub->mapFullPri[i]*Op->Sub->nDVect + k];



}

double AlgorithmDVS::norm_infty(int op,double *u_aprox)
{
   double norm;

   norm = Op->Sub->norma_A(op,u_aprox);

   norm = Op->call_norm_infty(norm);

   return norm;

}


AlgorithmDVS::~AlgorithmDVS() {
	// TODO Auto-generated destructor stub
}
