/*
 * DVS_BDDC.cpp
 *
 *  Created on: 18/06/2014
 *      Author: argonauta
 */

#include "DVS_BDDC.hpp"
#include "CGM.hpp"
#include "IterativeMethods.hpp"

DVS_BDDC::DVS_BDDC(OpInterface *_op) : AlgorithmDVS(_op) {
	// TODO Auto-generated constructor stub


}

void DVS_BDDC::ApplyOp(double *x, double *y){

	Apply_S(z, y);
	Op->calc_a(z);
	Apply_S_inv(x, z);
	Op->calc_a(x);

}

int DVS_BDDC::getSize(void){

	int val_ret = 0;

	val_ret = SchurComp->dimA;
	val_ret = Op->Sub->nDual;


	return val_ret;
}

double DVS_BDDC::dot(double *x, double *y){

 double dot_prod;
 double ret_dot;

 ret_dot = 0.;

 Apply_S(scr,y);

 dot_prod = SchurComp->dot(scr,x);

 ret_dot =  Op->call_dot(dot_prod);

 cout << " Valor del producto interior " << dot_prod << " " << ret_dot << endl;

 return ret_dot;
}

void DVS_BDDC::rhs(){

	double *f;

	f = new double[SchurComp->dimA*SchurComp->nDVect];

    SchurComp->rhs_SC(0,0,f);
    //<-- f_delta
	/*Apply_S_inv(rhss,f);
	Op->calc_a(rhss);*/

}

void DVS_BDDC::solve(){
int face,n_p;

double norm_err;
double norm_err_gl;

	//Define subsets of nodes to be used in the Schur complement.
	SchurComp->set_param_apply(Op->Sub->n_nodes,Op->Sub->mapDual,Op->Sub->nDual,Op->Sub->mapIntPri,Op->Sub->nIntPri,Op->Sub->Fem->FEM->nodo_sub);
	SchurComp->set_parameters_schur_int(Op->Sub->mapPrimal,Op->Sub->nPrim,Op->Sub->mapInt,Op->Sub->nInt);

	//Start arrays to be used in the iterative process.
    rhss = new double[SchurComp->dimA*SchurComp->nDVect];
    u = new double[SchurComp->dimA*SchurComp->nDVect];
    z = new double[SchurComp->dimA*SchurComp->nDVect];
    u_int = new double[SchurComp->dimA*SchurComp->nDVect];
    scr = new double[SchurComp->dimA*SchurComp->nDVect];

    for (int i = 0; i < SchurComp->dimA*SchurComp->nDVect; i++)
	  u[i] = u_int[i] = rhss[i] = 0.0;

	 //Check A_pipi

	 //SchurComp->apply_A_pipi();

     // Build right hand side for this Schur Complement.
	 rhs();

//	for (int i = 0; i < SchurComp->dimA*SchurComp->nDVect; i++)
	//	 u_int[i] = rhss[i];

	/* IterativeMethods *pt = (IterativeMethods *) this;

	 CG = new CGM(*pt, 1e-7);

     CG->rank = Op->P->rank;
	 CG->imprime = true;

	 CG->solve(u, rhss);

     Op->Sub->print_vector(Op->Sub->DUAL,u);*/

	 //if(Op->Sub->id_sub == 13)
/*	   for(int i = 0; i < SchurComp->dimA*SchurComp->nDVect ; i++)
	     cout << u[i] << std::endl; //<<  " " << rhss[i]<< endl;*/

	//norm_err = calc_err(u);
    //norm_err_gl =  Op->call_dot(norm_err);



	 //after finishing iterative procedure make substitution
}

DVS_BDDC::DVS_BDDC::~DVS_BDDC() {
	// TODO Auto-generated destructor stub
}
