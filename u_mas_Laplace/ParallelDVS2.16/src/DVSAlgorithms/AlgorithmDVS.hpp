/*
 * AlgorithmDVS.h
 *
 *  Created on: 31/03/2014
 *      Author: argonauta
 */


#include "OpInterface.hpp"
#include "Solvable.hpp"
#include "IterativeMethods.hpp"
#include "SchurComplement.hpp"
#include "SinPixSinPiySinPiz.h"


#ifndef ALGORITHMDVS_H_
#define ALGORITHMDVS_H_

class AlgorithmDVS : public IterativeMethods {
public:

    OpInterface *Op;
    Solvable *CG;
	SchurComplement *SchurComp;
    double *u;                             // vector to be applied.
	double *z;
	double *u_int;
    double *rhss;                          // right hand side of the equation.
	double *vv;
	double *ww;
	double *scr;                           // Scratch array
	double *uf;
	AlgorithmDVS(OpInterface *_op);
	//virtual void generate_rhs()=0;
	virtual void solve()=0; // entity that handles the whole procedure.
	virtual void rhs()=0;
	void Apply_S(double *v, double *w);
	void Apply_S_inv(double *v, double *w);
	void apply_j(double *x);
	double calc_err(double *u_aprox);
	double norm_infty(int op,double *u_aprox);
	virtual ~AlgorithmDVS();
};

#endif /* ALGORITHMDVS_H_ */
