#ifndef __DotProd__
#define __DotProd__




class DotProd
{

public:

   // returns  x (dot) y
   virtual double dot(double *x, double *y)=0;

};

#endif
