/*
 * BilinealGrad.cpp
 *
 *  Created on: 12/11/2010
 *      Author: argonauta
 */

#include "BilinealGrad.h"
#include <iostream>

BilinealGrad::BilinealGrad(int n) {
	// TODO Auto-generated constructor stub
  dim = n;
}

BilinealGrad::~BilinealGrad() {
	// TODO Auto-generated destructor stub
}

void BilinealGrad::_asigna_funciones(Func_Base *phi,Func_Base *psi)
{

	// TODO Auto-generated constructor stub
	this->phi_ = phi;
	this->psi_ = psi;


}

double BilinealGrad::evalua(double *x)
{
	int i;
	double val_ret=0;

	for(i=0;i<dim;i++)
	{
		 this->phi_->orden[i]=1;
		 this->psi_->orden[i]=1;
		 val_ret += this->phi_->evalua(x) * this->psi_->evalua(x); //asignar las derivadas y evaluar la funci�n.
		 this->phi_->orden[i]=0;
		 this->psi_->orden[i]=0;
	}

	//std::cout<< " valor op bil " << val_ret  <<std::endl;
	return val_ret;

}
