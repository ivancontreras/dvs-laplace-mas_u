#ifndef __VertPrimal__
#define __VertPrimal__

#include "Primal.hpp"


class VertPrimal : public Primal
{

public:

   const char *name;

   VertPrimal(void) : name("VertPrimal")
   { }

   inline bool isPrimal(int type, int *coordN, int *coordM)
   {
      //printf("%s",name);
      return (type & VERTEX) != 0;
   }

};

#endif
