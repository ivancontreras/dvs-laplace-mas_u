/*
 * SparseMatx.h
 *
 *  Created on: 19/03/2014
 *      Author: argonauta
 */

#ifndef SPARSEMATX_H_
#define SPARSEMATX_H_

class SparseMatx {

private:
	   int Col;
	   int Ren;
	   int Ban;

	   double **M; // matx
	   int **J; // columns by row
	   void alloc_memory(int ren,  int col,  int ban);


public:
	SparseMatx( int ren,  int col, int ban);
	void alloc( int ren,  int col,  double val);
	int num_col_ban(int ren);

	inline int col_number(int ren, int ind)
	   {
	      return (J[ren][ind]);
	   }

	 inline double ret_val_col(int ren, int ind)
	   {
	      return (M[ren][ind]);
	   }

	virtual ~SparseMatx();
};

#endif /* SPARSEMATX_H_ */
