/*
 * f_3D.h
 *
 *  Created on: 30/11/2010
 *      Author: argonauta
 */

#ifndef F_3D_H_
#define F_3D_H_
#include "Funcion.h"

class f_3D  : public Funcion{
public:
	f_3D();
	virtual ~f_3D();
	double evalua(double *x);

};

#endif /* F_3D_H_ */
