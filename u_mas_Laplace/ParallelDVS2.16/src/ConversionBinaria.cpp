/*
 * ConversionBinaria.cpp
 *
 *  Created on: 17/11/2010
 *      Author: argonauta
 */

#include "ConversionBinaria.h"

ConversionBinaria::ConversionBinaria() {
	// TODO Auto-generated constructor stub

}

ConversionBinaria::~ConversionBinaria() {
	// TODO Auto-generated destructor stub
}

 int *  ConversionBinaria::Convierte_a_binario(int dim,int valor)
	{

	int *binario;
	int cociente;
    int i;
	binario = new int[dim];
	cociente = valor;

    for(i=dim-1;i>=0;i--)
     {
	 binario[i] = cociente % 2;
	 cociente /= 2;
     }

	return binario;
	}
