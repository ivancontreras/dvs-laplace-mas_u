//============================================================================
// Name        : 15.cpp
// Author      : Iván Contreras
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "AlgorithmDVS.hpp"
#include "DVSSchurComplement.hpp"
#include "DVS_S_inv.hpp"
#include "DVS_BDDC.hpp"
#include "OpInterface.hpp"
#include "ParallelScheme.hpp"
#include "PropDef.hpp"
#include "SubDom.hpp"


#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {

	int *mesh;
	int nDim;

	/*Get Parameters */

	PropDef *props = new PropDef();
	props->parse(argc, argv);
	nDim = props->getInt("d", 0);

	mesh = new int[6];

	//Number of partitions in each axis for the coarse and the fine mesh.

	mesh[0] = props->getInt("Nx");
	mesh[1] = props->getInt("Ny");
	mesh[2] = props->getInt("Nz");
	mesh[3] = props->getInt("Mx");
	mesh[4] = props->getInt("My");
	mesh[5] = props->getInt("Mz");

	ParallelScheme P(argc, argv);
	SubDom SubDomain(P.rank,nDim,mesh);

	OpInterface Op(&P, &SubDomain);

	AlgorithmDVS *DVS_Alg;

	DVS_Alg = new DVS_BDDC(&Op);

	DVS_Alg->solve();

	P.stop_ParallelScheme();

}
