#ifndef __MultOp__
#define __MultOp__

class MultOp
{

public:

   /// y = A*x
   virtual void multOp(double *x, double *y)=0;

   /// vector size
   virtual int getSize(void)=0;

};

#endif
