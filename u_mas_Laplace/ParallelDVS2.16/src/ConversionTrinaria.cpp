/*
 * ConversionTrinaria.cpp
 *
 *  Created on: 16/11/2010
 *      Author: argonauta
 */

#include "ConversionTrinaria.h"
#include "./ConversionBinaria.h"
#include "math.h"
#include <iostream>

ConversionTrinaria::ConversionTrinaria(int dimension) {
	// TODO Auto-generated constructor stub
  dim = dimension;
}

ConversionTrinaria::~ConversionTrinaria() {
	// TODO Auto-generated destructor stub
}
int ConversionTrinaria::_num_nodo_soporte(int *z)
{
	int i,k;

    	for(i = 0;i<dim ; i++)
    		z[i] += 1;

    	 /*for(k=0;k<dim;k++)
    		std::cout << z[k] << " " ;
         std::cout << std::endl;*/

    	return valor_entero(z);
}
int ConversionTrinaria::valor_entero(int *z)
{
	 int val_ret,j,k;


	 val_ret = 0;


	 k=0;
	 for(j=dim-1;j>=0;j--)
	 {
	 	val_ret+= pow(3.0,k) * z[j];
	 	k++;
	 }

	 return val_ret;

  }

void ConversionTrinaria::asigna_signo(int *distancia, int *orientacion)
{
  int i;

  for(i=0;i<dim;i++)
    if(orientacion[i] ==1)
    	distancia[i] *=-1;

}

int * ConversionTrinaria::op_or_exclusivo(int x, int y)
{
    ConversionBinaria *ConBin;

    ConBin = new ConversionBinaria();
	int z = x ^ y;


	return ConBin->Convierte_a_binario(dim,z);

}
