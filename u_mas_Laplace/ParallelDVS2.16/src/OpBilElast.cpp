/*
 * OpBilElast.cpp
 *
 *  Created on: 28/10/2010
 *      Author: argonauta
 */

#include <iostream>
#include "OpBilElast.h"

OpBilElast::OpBilElast(int n) {
	// TODO Auto-generated constructor stub
	double alpha = -1e-3;
	double E_Poisson =  68.95e9;
	double nu = 26e-2;

	lambda = E_Poisson * nu;
	lambda /= (1+nu)*(1-2*nu);
	mu = E_Poisson ;
	mu /= 2*(1+nu);

    /*lambda = 1.0;
    mu = 1.0;*/
    comp = n;

}

OpBilElast::~OpBilElast() {
	// TODO Auto-generated destructor stub
}

double OpBilElast::evalua(double *x)
{
	double val_ret;
    int k;

	val_ret=0.0;
	//std::cin.get();
    //std::cout << " " << _grad_div(x) << " " << _div_grad(x) << " ";

	/*for(k=0;k<comp;k++)
		std::cout << " izq " << phi_->limites[k][0] << " der  " << phi_->limites[k][1] <<  " " << std::endl;*/

	val_ret = (lambda+mu)*_grad_div(x)+ (mu)*_div_grad(x);

	return  val_ret;

}

void OpBilElast::_asigna_derivadas(int i, int j){

	i_=i;
	j_=j;

}

void OpBilElast::_asigna_funciones(Func_Base *phi,Func_Base *psi)
{
	int k;
	// TODO Auto-generated constructor stub
	this->phi_ = phi;
	this->psi_ = psi;

}

double OpBilElast::_grad_div(double *coord){

     double val_ret;

	 this->phi_->orden[i_]=1;
	 this->psi_->orden[j_]=1;
	 val_ret = this->phi_->evalua(coord) * this->psi_->evalua(coord);
	 this->phi_->orden[i_]=0;
	 this->psi_->orden[j_]=0;
     return val_ret;
}

double OpBilElast::_div_grad(double *coord){

	double val_ret=0.0;
	int l;

		if(i_ == j_)
		 for(l=0;l<comp;l++)
		 {
			 this->phi_->orden[l]=1;
			 this->psi_->orden[l]=1;
			 val_ret += this->phi_->evalua(coord)*this->psi_->evalua(coord);
			 this->phi_->orden[l]=0;
			 this->psi_->orden[l]=0;

		      }

		return val_ret;

}

