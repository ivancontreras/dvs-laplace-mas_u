#ifndef __BandSolve__
#define __BandSolve__


#include "Solvable.hpp"
#include "SparseMatx.hpp"

/* Here a banded matrix aj is represented as a set of (2*bw + 1) columns by n
   rows. The diagonal aii is the column A[bw][i], and aij is A[bw + j - i][i]
   where |i - j| <= bw.
*/

class BandSolve : public Solvable
{

private:

   int bw;          // bandwidth
   double **AK;      // AK[2*bw + 1][n]


protected:

   void factorLU(void);

public:

   BandSolve(void)
   {
      name = "BandSolve";
      bw = n = 0;
      AK = 0;
   }

   BandSolve(int n, double **A);

   BandSolve(int n, SparseMatx *A);

   ~BandSolve()
   {
      clean();
   }

   void clean(void)
   {
      if (AK == 0) return;
      int i;
      for (i = 0; i < (2*bw + 1); i++)
      {
         delete []AK[i];
         AK[i] = 0;
      }
      delete []AK;
      AK = 0;
   }

   void solve(double *x, double *y);

   void convertBand(int n, double **A);

   void convertBand(int n, SparseMatx *A);

   void print(void);

   inline int getIter(void)
   {
      return 0;
   }
};

#endif
