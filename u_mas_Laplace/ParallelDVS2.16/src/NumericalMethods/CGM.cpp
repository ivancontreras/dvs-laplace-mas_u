#include <math.h>
#include "CGM.hpp"


void CGM::solve(double *u, double *b)
{
   int i;
   nIter = 1;
   double n_b;

   A->ApplyOp(v, u);    // v = A*u

  // n_b = norm(b);
  // mult_vect(b,1./n_b);

   for (i = 0; i < n; i++) p[i] = r[i] = b[i] - v[i];  //residuo


   gamma = A->dot(r,r);
    //mu = mu_0 = norm(r);
   mu = sqrt(gamma);


   //cout << "Valores al iniciar el CGM " << gamma << " " << mu << endl;

   while (mu > eps && nIter < nMaxIter)
     {

	   A->ApplyOp(v,p); //  v = A*p

	  lambda = A->dot(p, v);   // lambda = p*A*p

	   alpha = gamma/lambda;   // alpha = r*r/p*A*p

	  /*if(imprime && rank==13)
	    printf("\n Processor %d  nIter %d %d gamma %g lambda  %g alpha %g  beta %g",rank, nIter,nMaxIter, gamma, lambda,alpha ,beta);*/


	   for (i = 0; i < n; i++)
       {
     	 u[i] += alpha*p[i];
         r[i] -= alpha*v[i];
       }

    lambda = A->dot(r, r);

    beta = lambda/gamma;

    for (i = 0; i < n; i++)
     {
   	  p[i] = r[i] + beta*p[i];
      }

     gamma = lambda;

     mu = lambda;

	 nIter++;


	if(imprime)
      printf("\n Processor %d  nIter %d %d mu  %g %g \n ",rank, nIter,nMaxIter, mu, eps);



     }

  // mult_vect(u,n_b);


#ifdef RESIDUAL
   //printf("\nIterations:  %d, Error: %1.16f", nIter, mu);
   //fflush(stdout);
#endif
}


double CGM::norm(double *x)
{
   double v, val = 0.0;

   //printf("\nQue esta pasando con esta norma %d \n", n);
   for (int i = 0; i < n; i++)
      if ((v = fabs(x[i])) > val) val = v;
   return val;
}

void CGM::mult_vect(double *x,double n_f)
{


   //printf("\nQue esta pasando con esta norma %d \n", n);
   for (int i = 0; i < n; i++)
      x[i]*=n_f;

}
