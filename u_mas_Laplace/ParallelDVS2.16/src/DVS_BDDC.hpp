/*
 * DVS_BDDC.h
 *
 *  Created on: 18/06/2014
 *      Author: argonauta
 */

#ifndef DVS_BDDC_H_
#define DVS_BDDC_H_

#include "AlgorithmDVS.hpp"
#include "OpInterface.hpp"


class DVS_BDDC : public AlgorithmDVS {
public:
	DVS_BDDC(OpInterface *_op);
	void ApplyOp(double *x, double *y);
	double dot(double *x, double *y);
	void rhs();
	void solve();
	int getSize(void);
	virtual ~DVS_BDDC();
};

#endif /* DVS_BDDC_H_ */
