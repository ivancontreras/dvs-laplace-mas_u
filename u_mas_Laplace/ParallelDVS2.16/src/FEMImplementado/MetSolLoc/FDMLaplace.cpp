/*
 * FDMLaplace.cpp
 *
 *  Created on: Oct 11, 2016
 *      Author: ivanc
 */

#include "FDMLaplace.h"
#include "GeneraGeom.h"
#include "SinPixSinPiySinPiz.h"
#include "math.h"




FDM_Laplace::FDM_Laplace( GeneraGeom *geom) {
	// TODO Auto-generated constructor stub

	f_der = new f_2D;
	this->geom = geom;

	//Obtener el número total de nodos  el número total de grados de libertad.
	//En la clase geom ya están calculados esos valores.

	 tot_nodos = geom->count_nodos;// N�mero de nodos en el subdominio
	 tam_arreglo = (2*geom->dimension)+1; // N�mero de nodos en el soporte nodal.

	 coef = new double ***[tot_nodos];
	 nodo_sub = new int *[tot_nodos];
	 rhs = new double *[tot_nodos];


	 _fabrica_lader();

	 for(int k=0;k<tot_nodos;k++)
	  {

	    if(k==0)
	 	  coef[0] = new double **[tam_arreglo*tot_nodos];
	    else
	 	  coef[k] = coef[0] + k*tam_arreglo;

	     nodo_sub[k] = new int[tam_arreglo];

	 	 if(k==0)
	 	  rhs[0] = new double[tot_nodos*nDVect];
	     else
	 	  rhs[k] = rhs[0] + k*nDVect;

	 	  for(int j=0;j<tam_arreglo;j++)
	 	    {
	 	 	  if(j==0)
	 	 		 coef[k][0] = new double*[tam_arreglo*nDVect];
	 	 	   else
	 	 		 coef[k][j] = coef[k][0] + j*nDVect;

	 	 	   nodo_sub[k][j] = -1;

	 	 	   for(int i=0;i<nDVect;i++)
	 	 		{

	 	 		  if(i==0)
	 	 			coef[k][j][i] = new double[nDVect*nDVect];
	 	 		  else
	 	 			coef[k][j][i] = coef[k][j][0] + i*nDVect;


	 	 		  rhs[k][i] = 0.0 ;

	 	 		  for(int p=0;p < nDVect;p++)
	 	 			 coef[k][j][i][p] = 0.0;
	 	 			}

	 	 		 }

	 	 	 }


}


void FDM_Laplace::recorre_elementos()
{
	calcula_elemento();
	//calcula_rhs();

	//calcula_soporte();
   }

void FDM_Laplace::construye_sla(int _n_dof, int tot_nodos){

    int ren,col;

    int *coord;
    double *coor;

    coord = new int[geom->dimension];
    coor = new double[geom->dimension];

    //cout << " *************************** "<< _n_dof << " " << tot_nodos << endl;

    for(int i=0;i<tot_nodos;i++)
    {
        ren = geom->nodo_dof[i]->node_;

    	if( ren < 0 )
    	  continue;


    	  for(int j=0;j<tam_arreglo;j++)
    	  {
    		  col =  geom->nodo_dof[geom->nodo_dof[i]->estencil[j]]->node_;



    		  if(col >= 0)
    		  {
    			if(col == ren )
				 ;
    			else
    		      ;
    		  }
    	  }

    	  geom->genSubCoord(i,coord);
    	  geom->getCoord(coord,coor);


        }

}


void FDM_Laplace::_fabrica_lader()
{
	if(geom->dimension < 3)
	     f = new f_2D();
	  else
		 f = new f_3D();
}


void FDM_Laplace::calcula_elemento()
{


	 /**
	  *  Comienza el código que construye el sistema de ecuaciones local.
	  *  En este método se cálcula sólo el estencil.
	  */
	    int ren,col;
	    int m_pq;
	    int *coord;
	    double *coor;

	    SinPixSinPiySinPiz *exacta = new SinPixSinPiySinPiz();

	    coord = new int[geom->dimension];
	    coor = new double[geom->dimension];

	    for(int i=0;i<tot_nodos;i++)
	    {
	        ren = i;//geom->nodo_dof[i]->node_;
 // std::cout << std::endl;
	        for(int j=0;j<tam_arreglo;j++)
	          nodo_sub[i][j]= geom->nodo_dof[i]->estencil[j];

	    	/*if( ren < 0 )
	    	  continue;*/

	    	  for(int j=0;j<tam_arreglo;j++)
	    	  {
	    		//col =  geom->nodo_dof[geom->nodo_dof[i]->estencil[j]]->node_;

	    		col =  geom->nodo_dof[i]->estencil[j];

	    		  if(col >= 0)
	    		  {
                     // m_pq = geom->multiplicity[col];

                      m_pq = geom->_calc_m_p_q(i,col);

	    			  for(int p=0;p<nDVect;p++)
	    			    {

	    			  	  for(int q=0;q<nDVect;q++)
	    			  		{

	    			  		  if(col == ren )
	    			  			coef[i][j][p][q] =  1./m_pq + 4./(pow((double)geom->h[0],2)*m_pq);
	    		    		  else
	    		    		  {
                                   if( (geom->nodo_frontera[col]  & geom->KNOWN)  != 0)
                                   {

                                    geom->genSubCoord(col,coord);
                                	   geom->getCoord(coord,coor);
                                	   rhs[i][p] +=  exacta->evalua(coor)/(pow((double)geom->h[0],2)*this->geom->multiplicity[i]);
                                   }
                                   else
	    		    			    coef[i][j][p][q] =  -1./(pow((double)geom->h[0],2)*m_pq);


	    		    		  }

	    			  		  //std::cout << coef[i][j][p][q] << "  " ;
	    			  		}

      	    		  }

	    		  }



	        }


	    	  geom->genSubCoord(i,coord);
	    	  geom->getCoord(coord,coor);


	    	  /*std::cout<< "Coordenas " << geom->multiplicity[i] << " :: " << i ;

	    	     	    for(int j=0;j<2;j++)
	    	     	    	   std::cout<< "-- "<< coor[j]  ;

	    	     	   std::cout<< std::endl;*/


	    	  for(int p=0;p<nDVect;p++)
	    	  	{

	    		  rhs[i][p] +=  f_der->evalua(coor)/this->geom->multiplicity[i];
	    		  //std::cout << " rhs " << i <<  " " << this->geom->multiplicity[i] << " "<< rhs[i][p] << " " << - f_der->evalua(coor) << std::endl;
	    	  	}


            }



}





void FDM_Laplace::resuelve_sla(){



}


FDM_Laplace::~FDM_Laplace() {
	// TODO Auto-generated destructor stub

	  for(int k=0;k<tot_nodos;k++)
		 {

			for(int j=0;j<tam_arreglo;j++)
			  {

			 	for(int i=0;i<geom->dimension;i++)
		             delete []coef[k][j][i];

			 	delete [] coef[k][j];

			  }

			 delete []coef[k];
			 delete [] nodo_sub[k];
			 delete []rhs[k];

		  }

	    //delete []coef;
	    delete []nodo_sub;

}

void FDM_Laplace::solucion_sla(){

}







