/*
 * MetSolLocal.h
 *
 *  Created on: 22/10/2010
 *      Author: argonauta
 */

#ifndef METSOLLOCAL_H_
#define METSOLLOCAL_H_

#include <iostream>
#include "NodoG.h"

#include "GeneraGeom.h"

#include "f_2D.h"
#include "f_3D.h"

class MetSolLocal {
public:
	MetSolLocal();
	virtual ~MetSolLocal();
	virtual void recorre_elementos()=0;
	void calcula_limites(int *eje, double *coor, double *h);
	void despliega_matriz();
	void despliega_vector();
	void _fabrica_lader();

	double **limites; // almacena los extremos del segmento de un elemento.
	double ****coef; //almacena el c�lculo ; nodo a, nodo b , componente i , componente j.
	double **rhs; //lado derecho de la ecuaci�n.
	int **nodo_sub; //almacena el n�mero de nodo en la partici�n del subdominio.
	bool *frontera; // determina si un nodo en el subdominio pertenece a la frontera exterior.
	Funcion *f; //Funci�n que modela el lado derecho de la EDP
	int nDVect;
	int rank;
	int tot_nodos, tam_arreglo, num_nodos_elem;



};

#endif /* METSOLLOCAL_H_ */
