/*
 * FDMLaplace.h
 *
 *  Created on: Oct 11, 2016
 *      Author: ivanc
 */

#ifndef FEMIMPLEMENTADO_METSOLLOC_FDMLAPLACE_H_
#define FEMIMPLEMENTADO_METSOLLOC_FDMLAPLACE_H_

#include "MetSolLocal.h"
#include "f_2D.h"
#include "GeneraGeom.h"
#include <iostream>
#include <stdio.h>



class FDM_Laplace: public MetSolLocal {


	f_2D *f_der;

public:
	FDM_Laplace(GeneraGeom *geom);
	virtual ~FDM_Laplace();
	void obtiene_parametros();
	void resuelve_sla();
	void construye_sla(int _n_dof, int tot_nodos);
	void solucion_sla();
	void recorre_elementos();
	void _fabrica_lader();
	void calcula_elemento();
	GeneraGeom *geom;


};



#endif /* FEMIMPLEMENTADO_METSOLLOC_FDMLAPLACE_H_ */
