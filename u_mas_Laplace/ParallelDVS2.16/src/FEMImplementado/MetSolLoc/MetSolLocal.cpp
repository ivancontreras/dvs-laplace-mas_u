/*
 * MetSolLocal.cpp
 *
 *  Created on: 22/10/2010
 *      Author: argonauta
 */

#include "MetSolLocal.h"

MetSolLocal::MetSolLocal() {
	// TODO Auto-generated constructor stub

	 limites = new double *[3];

	 for(int i=0;i<3;i++)
	   limites[i] = new double[2];

	 nDVect = 1;

}

MetSolLocal::~MetSolLocal() {
	// TODO Auto-generated destructor stub
	int i,j,k;



	    /*for(int i=0;i<3;i++)
		   delete[] limites[i];

	     delete [] limites;

	for(k=0;k<tot_nodos;k++)
	 {

		for(j=0;j<tam_arreglo;j++)
		 {

			for(i=0;i<nDVect;i++)
				delete []coef[k][j][i];

			delete[]coef[k][j];

		 }
		delete[]coef[k];
		delete[]nodo_sub[k];
		delete[]rhs[k];
	 }

	delete[]coef;
	delete[]nodo_sub;
	delete[]rhs; */

}

void MetSolLocal::calcula_limites(int *eje, double *coor, double *h){

  /* Son los l�mites de un nodo de un elemento, la variable eje
   *  determina la orientaci�n del segmento en una dimensi�n dada.*/

   int i;


   for(i=0;i<3;i++)
   {
    if (eje[i] == 0)
     {
	    limites[i][1] = coor[i] + h[i];
	    limites[i][0] = coor[i];
	    //std::cout<< "Direcci�n;) " << i << " "<< eje[i] << " "<< coor[i]<< " " << limites[i][0] << " " << limites[i][1] << " ";

      }
     else
     {
   	   limites[i][1] = coor[i];
   	   limites[i][0] = coor[i]-h[i];
   	   //std::cout<<  "Direcci�n:( " << i << " "<< eje[i] << " "<< coor[i]<< " " << limites[i][0] << " " << limites[i][1] << " ";
       }
   //std::cout<< std::endl ;
   }
   //std::cout<< " cambio de nodo " <<std::endl ;
}


void MetSolLocal::despliega_vector()
{
	  for(int k=0;k<tot_nodos;k++)
	  {
		std::cout << k << " " ;

          for(int q=0;q<nDVect;q++)
			    std::cout << " "<<rhs[k][q]  << " ";
		   std::cout << std::endl;

	  }

	     //std::cin.get();


}

void MetSolLocal::despliega_matriz()
{

	std::cout << "Matriz Global  "  << tot_nodos << " " << tam_arreglo << " " << std::endl;
	  for(int k=0;k<tot_nodos;k++)
	  {
		std::cout << k << " " ;
	    for(int j=0;j<tam_arreglo;j++)
	     {
		  for(int p=0;p<nDVect;p++)
            for(int q=0;q<nDVect;q++)
			    std::cout << " "<<coef[k][j][p][q]  << " ";
		   std::cout << std::endl;
	        }

	     //std::cin.get();
	  }



}




