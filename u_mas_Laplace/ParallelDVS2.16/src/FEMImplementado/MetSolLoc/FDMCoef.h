/*
 * FDMCoef.h
 *
 *  Created on: Oct 23, 2016
 *      Author: ivanc
 */

#ifndef SRC_FDMCOEF_H_
#define SRC_FDMCOEF_H_

#include "GeneraGeom.h"
#include "FDMLaplace.h"
#include "MetSolLocal.h"



class FDMCoef {
public:
	FDMCoef(double **Subdom,int *particiones, int dim,double *h,bool imp, int *mult, int *type, int i_d);
	virtual ~FDMCoef();
	double *inf;
	double *sup;
	int dim;
	int *part;
	double *h1;
	int rank;
	MetSolLocal *FEM;
	GeneraGeom *GeomDisc;
};

#endif /* SRC_FDMCOEF_H_ */
