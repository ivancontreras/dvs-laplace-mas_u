/*
 * FDMCoef.cpp
 *
 *  Created on: Oct 23, 2016
 *      Author: ivanc
 */

#include "FDMCoef.h"

FDMCoef::FDMCoef(double **Subdom,int *particiones, int dimension,double *h, bool imp, int *mult,int *type, int i_d) {
	// TODO Auto-generated constructor stub
    int i;
    int partes;

	dim = dimension;
	inf = new double[dim];
	sup = new double[dim];
	part = new int[dim];
	h1 = new double[dim];

	 for(i=0;i<dim;i++)
	     {
	   	   inf[i] = Subdom[i][0];
	   	   sup[i] = Subdom[i][1];
	   	   part[i] = particiones[i];
	       h1[i] = h[i];

	   	        }

   partes = part[0];

   GeomDisc = new GeneraGeom(inf, sup, dim, partes, h1, mult,type, i_d);

   FEM = new FDM_Laplace(GeomDisc); // campo escalar

   // FEM = new FEMElastOp(Elementos); //El que se esta usando para sistemas de ecuaciones

   FEM->recorre_elementos();




}

FDMCoef::~FDMCoef() {
	// TODO Auto-generated destructor stub
	delete GeomDisc;
}

