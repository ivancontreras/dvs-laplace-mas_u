/*
 * Cuadratura.h
 *
 *  Created on: 16/10/2010
 *      Author: argonauta
 */

#ifndef CUADRATURA_H_
#define CUADRATURA_H_

#include "Funcion.h"
#include "CombinaCoef.h"

class Cuadratura {
public:
	Cuadratura(int n);
	~Cuadratura();
	CombinaCoef *combinaciones;
	double *coeficientes;
	double *raices;

	void ReInicia_(int n);
	void Inicia_Coef_Raices();
	double transformacion_lineal(double u,double *s);
	double cuadratura_gaussiana(double **x_,Funcion *f );

private:

	 int num_dim;


};

#endif /* CUADRATURA_H_ */
