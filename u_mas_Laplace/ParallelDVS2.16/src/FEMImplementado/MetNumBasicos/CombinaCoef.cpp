/*
 * CombinaCoef.cpp
 *
 *  Created on: 16/10/2010
 *      Author: argonauta
 */

#include "CombinaCoef.h"
#include <math.h>

CombinaCoef::CombinaCoef(int num_var_,int num_val) {
	// TODO Auto-generated constructor stub
	 int i;

	   a= num_val;
	   num_var = num_var_;
	   z = new int[num_var];
	   num=1;

	  for(i=0;i<num_var;i++)
	    z[i] = 0;
}

CombinaCoef::~CombinaCoef() {
	// TODO Auto-generated destructor stub
	delete []z;
}


void CombinaCoef::inicia_valores(int num_var_,int num_val)
{
	int i;

    a= num_val;
	num_var = num_var_;
	z = new int[num_var];
	num=1;

	for(i=0;i<num_var;i++)
	 z[i] = 0;

  }

bool  CombinaCoef::siguiente()
{

 if(num < pow(a,num_var))
   {
     if(debo_cambiar(0))
       if(z[0]<a)
         z[0] +=1;
       else
         z[0] = 0;

     num++;
     return true;
      }
 else
  return false;

  }

bool CombinaCoef::debo_cambiar(int i)
{
   int j;

   j = i + 1;

   if(i<num_var-1)
     if(debo_cambiar(j))
       if(z[i] < a-1 )
        {
	   z[i]+=1;
	   return false;
	    }
	else
	   {
            z[i] =0;
	     return true;
	     }
      else
        return false;
   else
      if(z[i] < a - 1 )
        {

	   z[i]+=1;
	   return false;
	    }
      else {

            z[i] =0;

	     return true;
	     }
      }

bool CombinaCoef::valor_booleano(int ai_bit)
{
	 bool val_ret;


	 if(z[ai_bit] == 0)
	 	val_ret = true;
	 else
	 	val_ret = false;

	 return val_ret;

}


int CombinaCoef::valor_entero(int ai_bit)
{
	 int val_ret,j,k;

	 val_ret = 0;


	 k=0;
	 for(j=num_var-1;j>=ai_bit;j--)
	 {
	 	val_ret+= pow(2.0,(double)k)*z[j];
	 	k++;
	 }

	 return val_ret;

  }

int CombinaCoef::valor_entero_bis(int ai_bit)
{
	 int val_ret,j,k;

	 val_ret = 0;


	 k=0;
	 for(j=0;j<=ai_bit;j++)
	 {
	 	val_ret+= pow(2.0,(double)k)*z[j];
	 	k++;
	 }

	 return val_ret;

  }

