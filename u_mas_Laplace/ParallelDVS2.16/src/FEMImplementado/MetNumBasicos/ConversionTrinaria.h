/*
 * ConversionTrinaria.h
 *
 *  Created on: 16/11/2010
 *      Author: argonauta
 */

#ifndef CONVERSIONTRINARIA_H_
#define CONVERSIONTRINARIA_H_

class ConversionTrinaria {
public:
	ConversionTrinaria(int dimension);
	virtual ~ConversionTrinaria();
	int valor_entero(int *ai_bit);
	int *op_or_exclusivo(int a, int b);
	void asigna_signo(int *distancia, int *orientacion);
	int _num_nodo_soporte(int *z);
	int dim;
};

#endif /* CONVERSIONTRINARIA_H_ */
