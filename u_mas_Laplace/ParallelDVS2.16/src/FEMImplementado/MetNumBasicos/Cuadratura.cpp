/*
 * Cuadratura.cpp
 *
 *  Created on: 16/10/2010
 *      Author: argonauta
 */

#include <iostream>
#include "Cuadratura.h"
#include "CombinaCoef.h"

Cuadratura::Cuadratura(int n) {

	 combinaciones = new CombinaCoef(n,3);
	 Inicia_Coef_Raices();

}

Cuadratura::~Cuadratura() {
	// TODO Auto-generated destructor stub
	delete combinaciones;
	delete []coeficientes;
	delete []raices;
}

void Cuadratura::ReInicia_(int n)
{
  delete combinaciones;
  combinaciones = new CombinaCoef(n,3);
  Inicia_Coef_Raices();

}

void Cuadratura::Inicia_Coef_Raices()
{
    coeficientes = new double[3];
    raices = new double[3];


    raices[2] = 0.7745966692;
    raices[1] = 0.0;
    raices[0] = -0.7745966692;
    coeficientes[2] = 0.5555555556;
    coeficientes[1] = 0.8888888889;
    coeficientes[0] =0.5555555556;
   }

double Cuadratura::transformacion_lineal(double u,double *s)
  {
     double retorno = ((u*(s[1]-s[0])) + s[1] + s[0])/2.0;

     return retorno;

       }

double Cuadratura::cuadratura_gaussiana(double **x_,Funcion *f )
   {

     double r,s,resultado;
     double *coor;
     int i;
     int count;

     s = 0.0;
     r = 1.0;

     coor = new double[combinaciones->num_var];

     /*multiplicacion de los limites de integraci�n*/
            for(i=0;i<combinaciones->num_var;i++)
              r *= ((x_[i][1] - x_[i][0])/2);

            count=1;

            do{
                resultado = 1.0;

               for(i=0;i<combinaciones->num_var;i++)
                {

            	  /*multiplicaci�n de los coeficientes*/
            	  resultado *= coeficientes[combinaciones->z[i]];
            	  coor[i] = transformacion_lineal(raices[combinaciones->z[i]],x_[i]);

              }


             //std::cout << "Evaluaci�n " << f->evalua(coor) << " " << std::endl;

              s +=  resultado*f->evalua(coor);


            }while(combinaciones->siguiente());


     return r*s;

   }
