/*
 * GeneraGeom.h
 *
 *  Created on: Oct 17, 2016
 *      Author: ivanc
 */

#ifndef GENERADISCRET_GENERAGEOM_H_
#define GENERADISCRET_GENERAGEOM_H_
#include "NodoG.h"


class GeneraGeom {

public:

	               // integer coordinates of node number (fine mesh)

	static const int KNOWN = 1, INTERIOR = 2, INTBD = 4, VERTEX = 8, EDGE = 16, FACE = 32, PRIMAL = 64, DUAL = 128;

	int dimension,partes,count_nodos, tot_incog; ///dimensi�n del espacio, n�mero de discretizaciones.

	int id;

	int *_rec; /// coordenadas corrimiento

    int *N;

	double *inf,*sup,*h;

	double **nodo; //el primer apuntador determina el n�mero de nodo y el segundo las componentes de las coordenadas del nodo.

	int *nodo_frontera; //Determina a que tipo de nodo en el dominio corresponde;

	Nodo_G **nodo_dof; //Almacena el n�mero de inc�gnita en el sistema.

	int *multiplicity;          // Number of subdomains wich one node belongs.

	GeneraGeom(double *infa, double *supb, int dim, int part, double *h1, int *mult,int *ntype, int i_d);

	int _frontera(int *coor);

	void genera_geometria();

	void calcula_estencil(int n,int *_coor,int *estencil);

	int _calc_m_p_q(int m, int q);

	void getCoord(int *coord, double *x);

	void genSubCoord(int n, int *coord);

	int _nodo_global(int *coor);

	virtual ~GeneraGeom();

};



#endif /* GENERADISCRET_GENERAGEOM_H_ */
