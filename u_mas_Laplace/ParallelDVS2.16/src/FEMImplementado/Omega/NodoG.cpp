/*
 * NodoG.cpp
 *
 *  Created on: Oct 17, 2016
 *      Author: ivanc
 */

#include "NodoG.h"
#include <iostream>
#include <stdlib.h>
using namespace std;




Nodo_G::Nodo_G(int dim, int n_dof) {
	// TODO Auto-generated constructor stub

	/** Cada vez que se construye uno de estos nodos se requiere llevar a cabo lo siguiente
	 * Determinar si es un nodo con grados de libertad.Por el momento todos los interiores lo son
	 * Determinar si es un nodo de frontera externa ; para esto requiere saber las dimesiones del dominio.
	 * Determinar el estencil; para ello requiere saber las coordenadas enteras del nodo.
	 */

	dimension = dim;
	this->node_ = n_dof;
	//cout << "Nodo incógnita " << n_dof<< endl ;
	this->estencil = new int[(2*dimension)+1];
}



Nodo_G::~Nodo_G() {
	// TODO Auto-generated destructor stub
}


