/*
 * NodoG.h
 *
 *  Created on: Oct 17, 2016
 *      Author: ivanc
 */

#ifndef GENERADISCRET_NODOG_H_
#define GENERADISCRET_NODOG_H_



class Nodo_G {
public:
	Nodo_G(int dim, int n_dof);

	int  node_;// número de grado de libertad,-1 indica que no hay dof ahí.
	int *_rec; /// coordenadas corrimiento
	int dimension;
	bool frontera_ext;
	int *estencil; //arreglo con el estencil , se guardan los números de nodos globales.



	virtual ~Nodo_G();
};



#endif /* GENERADISCRET_NODOG_H_ */
