/*
 * GeneraGeom.cpp
 *
 *  Created on: Oct 17, 2016
 *      Author: ivanc
 */

#include "GeneraGeom.h"
#include "NodoG.h"
#include <iostream>
#include <stdlib.h>
#include <math.h>
using namespace std;

GeneraGeom::GeneraGeom(double *infa, double *supb, int dim, int part, double *h1, int *mult,int *ntype, int i_d){
	// TODO Auto-generated constructor stub

	int i, n_dof;

    int *coord;

	double *coor;


	coor = new double[dimension];



	inf = infa;
	sup = supb;
	dimension = dim;
	partes = part;
	h = h1;
    count_nodos = 1;
    multiplicity = mult;
    nodo_frontera = ntype;
    id = i_d;


    //Particiones para cada eje

    N = new int[dimension];
    coord = new int[dimension];

    _rec = new int[2];

	_rec[0] = -1;
    _rec[1] = 1;

	//Calcular el n�mero de nodos totales
    for(i=0;i<dim;i++)
    {
       count_nodos *= (part + 1);
       N[i] = part+1;
       //cout << inf[i] << " " << sup[i] << endl;
    }

     //cout << " Total nodos "<< count_nodos << endl;

     nodo = new double *[count_nodos];
     //nodo_frontera = new int[count_nodos];

     nodo_dof = new  Nodo_G*[count_nodos];

     n_dof = 0;

     for(i=0;i<count_nodos;i++)
     {
    	 //calcula las coordenadas enteras del nodo

       genSubCoord(i,coord);
   	   getCoord(coord,coor);

/// Establish what type of node is every one of them

       if(_frontera(coord) != 1)
       {

       nodo_dof[i] = new Nodo_G(dimension,n_dof);
       calcula_estencil(i,coord,nodo_dof[i]->estencil);
       n_dof++;

       /// Include nodes with unknown values.

       }
       else
       {
    	   nodo_dof[i] = new Nodo_G(dimension,-1);
    	   calcula_estencil(i,coord,nodo_dof[i]->estencil);
       }



     }


      this->tot_incog = n_dof;


}

GeneraGeom::~GeneraGeom() {
	// TODO Auto-generated destructor stub
	 delete  []nodo_dof;
	 delete []N;
}

void GeneraGeom::genera_geometria(){

	/**con base en las particiones en cada eje calcula cuántos nodos globales hay.
	 */


}



int GeneraGeom::_frontera(int *coor){

	/* Valores para identificar los nodos en el dominio
	 * Frontera Neumann 2
	 * Frontera Dirichlet 1
	 * Interior 0
	 */
	int val_ret;

	val_ret = 0;

	if (coor[1] == 0 || coor[1] == partes)
	        val_ret = 1;

    if (coor[0] == 0 || coor[0] == partes)
    	val_ret = 1;


    return val_ret;

}


void GeneraGeom::getCoord(int *coord, double *x)
{



   for (int i = 0; i < dimension; i++)
	   x[i] = inf[i] + coord[i]*(sup[i]-inf[i])/(double)(N[i]-1);

}


void GeneraGeom::genSubCoord(int n, int *coord){

 int j=dimension - 1;

	   for (int i = dimension - 1; i >= 0; i--)
	   {
	      coord[j] = (i == 0 ? n : n / N[i - 1]);

	      if (i > 0) n -= coord[j]*N[i - 1];

	      j--;
	   }



}


int GeneraGeom::_nodo_global(int *coor){

	int num_nodo_gl=0;
	int calc;
	int tot_particiones;

	tot_particiones = partes;

	for(int i=0; i<dimension;i++)
	 {

	   calc = (int)pow( (double)(tot_particiones+1),(double)dimension-i-1)*coor[dimension-i-1]  ;
	   num_nodo_gl += calc;

			 }

       return num_nodo_gl;

 }

int GeneraGeom::_calc_m_p_q(int p, int q){

	if( multiplicity[p] >= multiplicity[q])
			return multiplicity[q];
	else
		return multiplicity[p];

}


void GeneraGeom::calcula_estencil(int n,int *_coor,int *estencil)
{

  int *_coor_rec;
  int _n;
  int nodo_est;
  double *coor;

  coor = new double[dimension];

  _coor_rec = new int[dimension];



  _n=0;

	for (int i=0; i<dimension ; i++) //recorre c/u de los ejes coordenados
	 {

		for (int k=0; k<dimension ; k++)
		   _coor_rec[k] =  _coor[k];

	   for (int j=0; j< 2 ; j++)  // recorre las coordenas corrimiento
	   {

		   _coor_rec[i] = _coor[i] + _rec[j];


		   if( _coor_rec[i] >= 0 && _coor_rec[i] <= partes )
		   {
			   /*for (int k=0; k<dimension ; k++)
                cout << " * " <<_coor_rec[k] << " * " ;*/

			   nodo_est = _nodo_global(_coor_rec); // calcula el número de nodo de las coordenadas obtenidas

			   //cout << " NG " << nodo_est ;
		   }
		   else
			   nodo_est = -1;

		   estencil[_n] = nodo_est;
		   _n++;
	     }

	   estencil[ dimension*2] = n;


	 }



 delete []_coor_rec;


}





