/*
 * Func_Tramo.h
 *
 *  Created on: 25/09/2010
 *      Author: argonauta
 *      Clase base de la que heredan las funciones definidas por tramos.
 *      Esto es que tienen valor en un nodo y su soporte pero fuera de el se desvanecen.
 *      En este caso estan construidas exclusivamente para un eje dimensional.
 */

#ifndef FUNC_TRAMO_H_
#define FUNC_TRAMO_H_
#include "Funcion.h"


class Func_Tramo : public Funcion {
public:
	Func_Tramo();
	virtual ~Func_Tramo();
	virtual double evalua_funcion(double *x)=0;
	virtual double evalua_derivada(double *x)=0;
	virtual double evalua_segunda_derivada(double *x)=0;
    int direccion, orden; // orientación del segmento, orden de la derivada a evaluar.
	double lim_der, lim_izq;
	double evalua(double *x);
	void establece_direccion(int dir);
	void establece_orden(int ord);
	void establece_limites(double izq, double der);

};

#endif /* FUNC_TRAMO_H_ */
