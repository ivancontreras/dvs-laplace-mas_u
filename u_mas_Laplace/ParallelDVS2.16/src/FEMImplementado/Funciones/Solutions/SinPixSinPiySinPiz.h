/*
 * SinPixSinPiySinPiz.h
 *
 *  Created on: 26/05/2014
 *      Author: argonauta
 */



#ifndef SINPIXSINPIYSINPIZ_H_
#define SINPIXSINPIYSINPIZ_H_


#include "Funcion.h"

class SinPixSinPiySinPiz : public Funcion {
public:
	SinPixSinPiySinPiz();
	double evalua(double *x);
	virtual ~SinPixSinPiySinPiz();
};

#endif /* SINPIXSINPIYSINPIZ_H_ */
