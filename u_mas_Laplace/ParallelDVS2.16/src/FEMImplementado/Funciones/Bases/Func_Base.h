/*
 * Func_Base.h
 *
 *  Created on: 23/10/2010
 *      Author: argonauta
 */

#include "Funcion.h"
#include "Func_Tramo.h"

#ifndef FUNC_BASE_H_
#define FUNC_BASE_H_

class Func_Base : public Funcion{
public:
	Func_Base(Func_Tramo *f_p,int dim);
	virtual ~Func_Base();

	double evalua(double *x);
	void establece_direccion(int *dir);
	void establece_orden(int *ord);
	void establece_limites(double **lim);
	int *direccion, *orden; // orientación del segmento, orden de la derivada a evaluar.
	int dim; // dimensión del espacio.
	double **limites;

	Func_Tramo *f;

};

#endif /* FUNC_BASE_H_ */
