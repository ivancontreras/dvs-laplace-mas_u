/*
 * f_2D.cpp
 *
 *  Created on: 29/11/2010
 *      Author: argonauta
 */

#include "f_2D.h"
#include "math.h"
#include <iostream>

f_2D::f_2D() {
	// TODO Auto-generated constructor stub

}

f_2D::~f_2D() {
	// TODO Auto-generated destructor stub
}

double f_2D::evalua(double *x)
{
	double val_ret=0;

	/* para el ejemplo de sen pi x sen pi y*/

	val_ret = 2.0*pow(3.14159,2);

	for(int i=0;i<2;i++)
		val_ret *= sin(3.14159 * x[i]);

	//std::cout << " Funci�n lado derecho " << val_ret << std::endl;

	return val_ret;


}


