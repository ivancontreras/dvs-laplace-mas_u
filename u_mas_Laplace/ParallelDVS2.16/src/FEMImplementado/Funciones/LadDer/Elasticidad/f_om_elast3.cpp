/*
 * f_om_elast3.cpp
 *
 *  Created on: 27/12/2010
 *      Author: argonauta
 */

#include "f_om_elast3.h"

f_om_elast3::f_om_elast3(double lambda, double mu) {
	// TODO Auto-generated constructor stub
	  this->lambda = lambda;
	  this->mu = mu;

}

f_om_elast3::~f_om_elast3() {
	// TODO Auto-generated destructor stub
}

double f_om_elast3::evalua(double *x)
{
	double val_ret,val_prov;

	val_ret = -(lambda+mu);

	val_prov =   cos( M_PI* x[0])* sin( M_PI* x[1])* cos( M_PI* x[2]);
	val_prov +=  sin( M_PI* x[0])* cos( M_PI* x[1])* cos( M_PI* x[2]);
	val_prov -=  sin( M_PI* x[0])* sin( M_PI* x[1])* sin( M_PI* x[2]);
	val_prov *=  pow( M_PI,2);

	val_ret *= val_prov;
	val_prov=0.0;
	val_prov -=  sin( M_PI* x[0])* sin( M_PI* x[1])* sin( M_PI* x[2]);
	val_prov -=  sin( M_PI* x[0])* sin( M_PI* x[1])* sin( M_PI* x[2]);
	val_prov -=  sin( M_PI* x[0])* sin( M_PI* x[1])* sin( M_PI* x[2]);
	val_prov *=  pow( M_PI,2);

	val_prov *= -mu;
	val_ret += val_prov;

	return val_ret;

}
