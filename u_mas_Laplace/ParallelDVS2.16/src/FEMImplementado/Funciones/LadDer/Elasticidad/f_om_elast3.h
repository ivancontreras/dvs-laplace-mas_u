/*
 * f_om_elast3.h
 *
 *  Created on: 27/12/2010
 *      Author: argonauta
 */

#ifndef F_OM_ELAST3_H_
#define F_OM_ELAST3_H_

#include "Funcion.h"
#include "math.h"
#include <iostream>

class f_om_elast3 : public Funcion{
public:
	f_om_elast3(double lambda, double mu);
	virtual ~f_om_elast3();
	double evalua(double *x);
	double lambda,mu;
};

#endif /* F_OM_ELAST3_H_ */
