/*
 * f_2D.h
 *
 *  Created on: 29/11/2010
 *      Author: argonauta
 */

#ifndef F_2D_H_
#define F_2D_H_

#include "Funcion.h"



class f_2D  : public Funcion {
public:
	f_2D();
	virtual ~f_2D();
	double evalua(double *x);

};

#endif /* F_2D_H_ */
