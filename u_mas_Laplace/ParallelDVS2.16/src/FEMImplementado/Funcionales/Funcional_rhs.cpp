/*
 * Funcional_rhs.cpp
 *
 *  Created on: 21/11/2010
 *      Author: argonauta
 */

#include "Funcional_rhs.h"
#include <iostream>

Funcional_rhs::Funcional_rhs(int n) {
	// TODO Auto-generated constructor stub
   this->dim = n;
}

Funcional_rhs::~Funcional_rhs() {
	// TODO Auto-generated destructor stub
}

void Funcional_rhs::_asigna_funciones(Funcion *f,Func_Base *psi)
{

	// TODO Auto-generated constructor stub
	this->f = f;
	this->psi_ = psi;


}

double Funcional_rhs::evalua(double *x)
{
	double val_ret=0;

 	val_ret = this->psi_->evalua(x)  * this->f->evalua(x);

	return val_ret;

}
