/*
 * Forma_Bilineal.h
 *
 *  Created on: 20/10/2010
 *      Author: argonauta
 *      De esta clase heredan todos las clases que modelan alguna formulación variacional.
 *
 */

#ifndef FORMA_BILINEAL_H_
#define FORMA_BILINEAL_H_

#include "Funcion.h"
#include "Func_Base.h"

class Forma_Bilineal: public Funcion {
public:
	Forma_Bilineal();
	virtual ~Forma_Bilineal();
	//void establece_orden(int *ord);
	int *direccion, *orden; // orientación del segmento, orden de la derivada a evaluar.
	int dim; // dimensión del espacio.
    Func_Base *phi_,*psi_;
    Funcion *f;


};

#endif /* FORMA_BILINEAL_H_ */
