/*
 * OpMatxVect.h
 *
 *  Created on: 18/03/2014
 *      Author: argonauta
 */

#ifndef OPMATXVECT_H_
#define OPMATXVECT_H_

#include "SparseMatx.hpp"
#include "Solvable.hpp"
#include "BandSolve.hpp"
#include <iostream>
using namespace std;

class OpMatxVect {

protected:
	Solvable *solve;

public:
	SparseMatx *A_NN;
	int **nodo_sub;
	int num_nodes;
	int support; // number of elements different to zero that are associated with any node.
	static const int KNOWN = 1, INTERIOR = 2, INTBD = 4, VERTEX = 8, EDGE = 16, FACE = 32, PRIMAL = 64, DUAL = 128;
	double ****coef;
	double **rhs;
	int *ntype;
	int dimA,dimV;
	int nDVect;
	int *mapA;
	int *mapV;
	OpMatxVect(int dim, int stencil, int *ntp);
	void applyMtxVect(double *w,double *v,int *mA, int *mV);
	void inverse_A_NN(int *mV, int dV);
	void inverse(double *v,double *w);
    void MatxVectValues(double ****A, double **V);
    void recover_rhs(double *v, int *mA, double *v1, int *mapS);
    void recover_rhs(double *v, int *mA,double *v1);
    void recover_rhs(double *v,int *mA);
    void difference_vectors(double *v,double *w, int tam); // obtain difference of two vectors of the same size.
    double calc_norm_square();
	void set_param_apply(int n_nodes,int *mA,int numA, int *mV,int numV,int **n_sub); // this information comes from the discretization method (FEM)

	virtual ~OpMatxVect();
};

#endif /* OPMATXVECT_H_ */
