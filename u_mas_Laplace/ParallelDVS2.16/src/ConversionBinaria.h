/*
 * ConversionBinaria.h
 *
 *  Created on: 17/11/2010
 *      Author: argonauta
 */

#ifndef CONVERSIONBINARIA_H_
#define CONVERSIONBINARIA_H_

class ConversionBinaria {
public:
	ConversionBinaria();
	virtual ~ConversionBinaria();
	int * Convierte_a_binario(int dim,int valor);
};

#endif /* CONVERSIONBINARIA_H_ */
