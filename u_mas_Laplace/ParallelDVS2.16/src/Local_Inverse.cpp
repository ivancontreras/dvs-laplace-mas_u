/*
 * Local_Inverse.cpp
 *
 *  Created on: 24/08/2014
 *      Author: argonauta
 */

#include "Local_Inverse.hpp"
#include "OpMatxVect.hpp"
#include "CGM.hpp"

Local_Inverse::Local_Inverse(OpMatxVect *Op_Mv) {
	// TODO Auto-generated constructor stub
	 OpMtxV = Op_Mv;
}

void Local_Inverse::ApplyOp(double *x, double *y){


	OpMtxV->applyMtxVect(x,y,map_,map_);

}

double Local_Inverse::dot(double *u, double *v){

	   double val = 0.0;
	   for (int i = 0; i < tam*OpMtxV->nDVect; i++) val += u[i]*v[i];
	   return val;

}

void Local_Inverse::inverse_A_NN(int *mV, int dV){

	map_ = mV;
	tam = dV;

}

void Local_Inverse::inverse(double *v,double *w){

    Solvable *CG;
    double *X;
	double *Y;
	int i,j;

    //IterativeMethods *pt = (IterativeMethods *) this;
    X = new double[tam* OpMtxV->nDVect];
    Y = new double[tam*OpMtxV->nDVect];

	for (i = 0; i < tam*OpMtxV->nDVect; i++) X[i] = Y[i] = 0.0;

	for (i = 0; i < tam*OpMtxV->nDVect; i++)  Y[i] = w[i];

	CG = new CGM(*this, 1e-6);
    CG->solve(X, Y);

    for (i = 0; i < tam*OpMtxV->nDVect; i++)
    	{
    	 //cout << " CGM interior " << i << " " <<  tam << " " <<v[i] << endl;
    	 v[i] = X[i];
    	}

	//pt = 0;
	//delete pt;
    delete []X;
   	delete []Y;
    delete CG;

}
int Local_Inverse::getSize(void){

	return tam;
}

Local_Inverse::~Local_Inverse() {
	// TODO Auto-generated destructor stub
}
