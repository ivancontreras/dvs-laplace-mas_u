#ifndef __CGM__
#define __CGM__

#include "Solvable.hpp"
#include "IterativeMethods.hpp"

class CGM : public Solvable
{

protected:

   // scratch vectors
   double *r, *p,*w;
   // scratch scalars
   double alpha, beta, gamma, lambda;
   // allowed error
   double eps;

   double mu;
   double mu_0;

   // Operator and Inner Product
   IterativeMethods *A;
   // number of iterations
   int nIter;
   // Numero maximo de iteraciones
   int nMaxIter;


   double norm(double *x);
   void mult_vect(double *x,double n_f);

public:


   // size of vector
   int nDVect;
   double *v;

   CGM(void)
   {
      nMaxIter = 200;
      this->eps = 10e-7;
      r = 0;
      p = 0;
      v = 0;
      w = 0;
   }


   CGM(IterativeMethods &A, double eps)
   {

      r = 0;
      p = 0;
      v = 0;
      w = 0;

      name = "CGM";
      nMaxIter = 200;
      this->A = &A;
      this->eps = eps;
      nDVect = 1;
      n = A.getSize()*nDVect;
      inicializa();
   }


   ~CGM()
   {
      //clean();
   }

   void clean(void)
   {
      delete []r;
      r = 0;
      delete []p;
      p = 0;
      delete []v;
      v = 0;
   }


   void inicializa(void)
   {
      r = new double[n];

      p = new double[n];

      v = new double[n];

      w = new double[n];

      for (int i = 0; i < n; i++)
    	  r[i] = p[i] = v[i] = w[i] = 0.0;
   }


   void solve(double *u, double *b);

   int getIter(void)
   {
      return nIter;
   }


   void setMaxIter(int nmi)
   {
      nMaxIter = nmi;
   }

   void setEpsilon(double ep)
   {
      eps = ep;
   }

};

#endif
