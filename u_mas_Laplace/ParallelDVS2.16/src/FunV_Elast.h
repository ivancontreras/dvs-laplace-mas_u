/*
 * FunV_Elast.h
 *
 *  Created on: 27/12/2010
 *      Author: argonauta
 */

#ifndef FUNV_ELAST_H_
#define FUNV_ELAST_H_

#include "Funcion.h"



class FunV_Elast : public Funcion{
public:
	FunV_Elast();
	virtual ~FunV_Elast();
	double evalua(double *x);
	Funcion *f_der[3];

};

#endif /* FUNV_ELAST_H_ */
