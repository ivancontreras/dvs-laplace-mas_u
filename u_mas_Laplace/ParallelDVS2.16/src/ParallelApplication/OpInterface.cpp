/*
 * OpInterface.cpp
 *
 *  Updated on: 03/04/2014
 *      Author: argonauta
 */

#include "OpInterface.hpp"

OpInterface::OpInterface(ParallelScheme *_p, SubDom *_sub){
	// TODO Auto-generated constructor stub
	P = _p;
	Sub = _sub;
}


void OpInterface::build_array_values_a( int _source, int _dest){


	//count nodes in source Face
	tot_source = 0;

	for(int i=0; i < Sub->k_[_source];i++)
	  	if( (Sub->ntype[Sub->node_face[_source][i]] & Sub->DUAL)!= 0 )
	  		tot_source++;



	//count nodes in dest Face
	tot_dest = 0;

	for(int i=0; i < Sub->k_[_dest];i++)
	  	if(  (Sub->ntype[Sub->node_face[_dest][i]] &  Sub->DUAL)!= 0)
	  		tot_dest++;

    arr_send = new double[tot_dest*Sub->nDVect];

	for(int i=0; i < tot_dest*Sub->nDVect;i++)
	   arr_send[i] = 0.;

	arr_receive = new double[tot_source*Sub->nDVect];

   	for(int i=0; i <tot_source*Sub->nDVect;i++)
	   arr_receive[i] = 0.;

}


void OpInterface::build_array_values_a_prime( int _source, int _dest){


	//count nodes in source Face
	tot_source = 0;



	for(int i=0; i < Sub->k_[_source];i++)
	  	if( (Sub->ntype[Sub->node_face[_source][i]] & Sub->PRIMAL)!= 0 )
	  		tot_source++;



	//count nodes in dest Face
	tot_dest = 0;

	for(int i=0; i < Sub->k_[_dest];i++)
	  	if(  (Sub->ntype[Sub->node_face[_dest][i]] &  Sub->PRIMAL)!= 0)
	  		tot_dest++;

    arr_send = new double[tot_dest*Sub->nDVect];

	for(int i=0; i < tot_dest*Sub->nDVect;i++)
	   arr_send[i] = 0.;

	arr_receive = new double[tot_source*Sub->nDVect];

   	for(int i=0; i < tot_source*Sub->nDVect;i++)
	   arr_receive[i] = 0.;

}

void OpInterface::calc_a( double *u_){

	// average
	build_array_values_a(LEFT,RIGHT);
	send_vect_a(RIGHT,u_);
	exchange_vector( LEFT, RIGHT);
	calc_a_vect( LEFT , u_);
	clean_array_values();


	// return value
	build_array_values_a(RIGHT,LEFT);
	send_vect_a(LEFT, u_);
	exchange_vector( RIGHT, LEFT);
	receive_vect_a(RIGHT,u_);
	clean_array_values();


	//average
    build_array_values_a(DOWN,UP);
    send_vect_a(UP,u_);
	exchange_vector( DOWN, UP);
	calc_a_vect( DOWN , u_);
	clean_array_values();

	// return value
	build_array_values_a(UP,DOWN);
	send_vect_a(DOWN,u_);
	exchange_vector( UP, DOWN);
	receive_vect_a(UP,u_);
	clean_array_values();

	/*if ( Sub->nDim >2)
	{
	// average
    build_array_values_a(BACK,FRONT);
    send_vect_a(FRONT,u_);
	exchange_vector( BACK, FRONT);
	calc_a_vect(BACK ,u_);
	clean_array_values();

	//return
	build_array_values_a(FRONT,BACK);
	send_vect_a(BACK,u_);
	exchange_vector( FRONT, BACK);
	receive_vect_a(FRONT,u_);
	clean_array_values();
	}*/

}


void OpInterface::calc_a_prime( double *u_){

	// average
	build_array_values_a_prime(LEFT,RIGHT);
	send_vect_a_prime(RIGHT,u_);
	exchange_vector( LEFT, RIGHT);
	calc_a_prime_vect( LEFT , u_);
	clean_array_values();


	// return value
	build_array_values_a_prime(RIGHT,LEFT);
	send_vect_a_prime(LEFT, u_);
	exchange_vector( RIGHT, LEFT);
	receive_vect_a_prime(RIGHT,u_);
	clean_array_values();


	//average
    build_array_values_a_prime(DOWN,UP);
    send_vect_a_prime(UP,u_);
	exchange_vector( DOWN, UP);
	calc_a_prime_vect( DOWN , u_);
	clean_array_values();

	// return value
	build_array_values_a_prime(UP,DOWN);
	send_vect_a_prime(DOWN,u_);
	exchange_vector( UP, DOWN);
	receive_vect_a_prime(UP,u_);
	clean_array_values();

	// average
/*	if ( Sub->nDim >2)
    {
	build_array_values_a_prime(BACK,FRONT);
    send_vect_a_prime(FRONT,u_);
	exchange_vector( BACK, FRONT);
	calc_a_prime_vect(BACK ,u_);
	clean_array_values();

	//return
	build_array_values_a_prime(FRONT,BACK);
	send_vect_a_prime(BACK,u_);
	exchange_vector( FRONT, BACK);
	receive_vect_a_prime(FRONT,u_);
	clean_array_values();
    }
*/


}


double OpInterface::call_dot(double val){

	double tot_dot;

	tot_dot = 0.;

	MPI_Allreduce(&val, &tot_dot, 1, MPI_DOUBLE, MPI_SUM,
	                MPI_COMM_WORLD);

	 return tot_dot;
}


double OpInterface::call_norm_infty(double val){


	double tot_dot;

	tot_dot = 0.;

	MPI_Allreduce(&val, &tot_dot, 1, MPI_DOUBLE, MPI_MAX,
	                MPI_COMM_WORLD);

	return tot_dot;


}

void OpInterface::calc_a_vect( int _source , double *u_){

	//This method calculates average between two subdomains that share an interface node labeled as DUAL.
	int n_arr;
	int n_p;
    int val;

	n_arr = 0;
	n_p = 0;



   for(int i=0; i < Sub->k_[_source];i++)
		{

   		  n_p =  Sub->node_face[_source][i];

		   if((Sub->ntype[n_p] & Sub->DUAL)!= 0)
		    {

			    for(int j=0;j<Sub->nDVect;j++)
			    {
			    	 //cout << Sub->k_[_source] << " " << n_p << " " << Sub->node_dual[n_p] << " " << Sub->nDVect  << " " << j << endl;
			          u_[Sub->node_dual[n_p]*Sub->nDVect+j] += arr_receive[n_arr*Sub->nDVect+j];
			          u_[Sub->node_dual[n_p]*Sub->nDVect+j] /= 2.0;
			    }

			  n_arr++;
		    }

		 }
}


void OpInterface::calc_a_prime_vect( int _source , double *u_){

	//This method calculates average between two subdomains that share an interface node labeled as PRIMAL.
	int n_arr;
	int n_p;

	n_arr = 0;
	n_p = 0;

    for(int i=0; i < Sub->k_[_source];i++)
		{
		  n_p =  Sub->node_face[_source][i];

		   if((Sub->ntype[n_p] & Sub->PRIMAL)!= 0)
		    {

			    for(int j=0;j<Sub->nDVect;j++)
			    {

			          u_[Sub->node_primal[n_p]*Sub->nDVect+j] += arr_receive[n_arr*Sub->nDVect+j];
			          u_[Sub->node_primal[n_p]*Sub->nDVect+j] /= 2.0;

			    }

			  n_arr++;
		    }


		 }

}

void OpInterface::clean_array_values(){

	delete []arr_send;
	delete []arr_receive;

}

void   OpInterface::exchange_vector( int _source, int _dest){


	int length_s, length_d;


	length_s = tot_source * Sub->nDVect;
	length_d = tot_dest * Sub->nDVect;
	 //From  _source to _dest
	 P->dest = P->nbrs[_dest];
	 P->source = P->nbrs[_source];
	 P->outbuf = P->rank;


    MPI_Isend(arr_send, length_d, MPI::DOUBLE, P->dest, P->tag,
			   MPI_COMM_WORLD, &P->reqs[0]);
    MPI_Irecv(arr_receive, length_s, MPI::DOUBLE, P->source, P->tag,
			   MPI_COMM_WORLD, &P->reqs[1]);

    MPI_Waitall(2, P->reqs, P->stats) ;

    // cout << " Sub " << P->rank << " enviando a " << P->nbrs[_dest] <<  " cara " << _dest << " nodos " << tot_dest <<endl;





}

void OpInterface::receive_vect_a( int _source , double *u_){

	int n_arr;
	int n_p;

	n_arr = 0;
	n_p = 0;

    for(int i=0; i < Sub->k_[_source];i++)
		{
		  n_p =  Sub->node_face[_source][i];


		  if((Sub->ntype[n_p] & Sub->DUAL)!= 0)
		  {
			for(int j=0;j<Sub->nDVect;j++)
			 u_[Sub->node_dual[n_p]*Sub->nDVect+j] = arr_receive[n_arr*Sub->nDVect+j] ;

			n_arr++;
		    }

		 }

 }

void OpInterface::receive_vect_a_prime( int _source , double *u_){

	int n_arr;
	int n_p;

	n_arr = 0;
	n_p = 0;

    for(int i=0; i < Sub->k_[_source];i++)
		{
		  n_p =  Sub->node_face[_source][i];


		  if((Sub->ntype[n_p] & Sub->PRIMAL)!= 0)
		  {
			for(int j=0;j<Sub->nDVect;j++)
			  u_[Sub->node_primal[n_p]*Sub->nDVect+j] = arr_receive[n_arr*Sub->nDVect+j] ;

			n_arr++;
		    }

		 }

 }

//This method receive as parameter u_ a vector with all the entries for an specific kind of nodes.
void OpInterface::send_vect_a( int _dest , double *u_){


	int n_p;
	int n_arr;
	n_arr = 0;

	for(int i=0; i < Sub->k_[_dest];i++)
	{
	  n_p =  Sub->node_face[_dest][i]; // number of node in fine mesh.


	   if(  (Sub->ntype[n_p] & Sub->DUAL)!= 0)
	    {

		  for(int j=0;j<Sub->nDVect;j++)

                 arr_send[n_arr*Sub->nDVect+j] = u_[Sub->node_dual[n_p]*Sub->nDVect+j];


			 /*if(P->rank == 4)
			   for(int j=0;j<Sub->nDVect;j++)
				 cout <<  arr_send[n_arr*Sub->nDVect+j] << endl;*/

		   n_arr++;
	    }


	}

    /*if(P->rank == 4)
       cout << endl << _dest << " " << P->rank<<" " << P->nbrs[_dest] << "********************************Enviado********************************" << endl;*/

}

void OpInterface::send_vect_a_prime( int _dest , double *u_){


	int n_p;
	int n_arr;
	n_arr = 0;

	for(int i=0; i < Sub->k_[_dest];i++)
	{
	  n_p =  Sub->node_face[_dest][i]; // number of node in fine mesh.



	   if(  (Sub->ntype[n_p] & Sub->PRIMAL)!= 0)
	    {

		  for(int j=0;j<Sub->nDVect;j++)
                 arr_send[n_arr*Sub->nDVect+j] = u_[Sub->node_primal[n_p]*Sub->nDVect+j];


			 if(P->rank == -1)
			   for(int j=0;j<Sub->nDim;j++)
				 cout <<  n_arr*Sub->nDVect+j << "  " << arr_send[n_arr*Sub->nDVect+j] << endl;

		   n_arr++;
	    }


	}

    if(P->rank == -10)
       cout << endl << _dest << " " << P->rank<<" " << P->nbrs[_dest] << "********************************Enviado********************************" << endl;

}


OpInterface::~OpInterface() {
	// TODO Auto-generated destructor stub
}
