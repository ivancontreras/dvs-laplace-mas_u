/*
 * SubDom.h
 *
 *  Created on: 21/02/2014
 *      Author: argonauta
 */

#include "AllPrimal.hpp"
#include "FDMCoef.h"
#include "Mesh.hpp"
#include "NoPrimal.hpp"
#include "Primal.hpp"
#include "VertPrimal.hpp"
#include "VertEdgePrimal.hpp"

#include <iostream>
#include <math.h>
#include <fstream>
#include <sstream>
#include <stdio.h>


using namespace std;


#ifndef SUBDOM_H_
#define SUBDOM_H_


class SubDom : public Mesh{

private:

	   int beta;
	   int *multiplicity;          // Number of subdomains wich one node belongs.
	   double *x;
	   void defineFace();
	   void genNtype();
	   int nodeType(int *coord);
	   void generate_subsets();


public:

	int id_sub;
	int nDVect;  				// it says how many entries will have a vector on each node.
	int n_nodes;               // total node size (Mx +1) or (Mx + 1)(My + 1) or (Mx + 1)*(My + 1)*(Mz + 1)
	int l_dual, l_primal;
	int *k_;
	int **node_face;           //nodes associated with one face.
	int *node_primal;
	int *node_dual;
    int **nodes_subdomains;    // List of subdomains in the neighborhood of one node.
	double *u_pi;
	double *u_delta;
	int *ntype;                // Describes type of each node
	int *mapInt;               // equation mapping for interior matrix
	int *mapPrimal;			  // equation mapping for Primal nodes
	int *mapFull;              // equation mapping for full (minus primals) matrix
	int *mapIntPri;			  // equation mapping for interior and primal nodes. It means Primal + Interior.
	int *mapFullPri;			  // equation mapping for Boundary and Primal nodes.
	int *mapDual;			  // equation mapping for Dual nodes
	int *mapA;                 // subset of nodes to be computed as rows of A_MN
	int *mapV;                 // subset of nodes to be computed as columns of A_MN
	int nA;					  // number of rows
	int nV;					  // number of columns
	int nInt;                   // number of interior nodes (for mapInt)
	int nFull;                  // number of interior + dual nodes
	int nIntPri;                // number of interior + primal nodes
	int nPrim;				   // number of Primal nodes.
	int nBdPrim;				   // number of nodes in Boundary.
	int nDual;			   // number of Primal nodes.
	Primal *primal;
	FDMCoef *Fem;
	SubDom(int id, int nDim, int *mesh);
	void assign_vector_values(int op, double *u_,int *node_);
	void display_faces();
	void display_face_vect(int op, int _source , double *u_, int *node_);
	double norma_A(int op, double *u_);
	void print_interface(int op, int _source , double *u_, int *node_);
	void print_vector(int op, double *u_);
	void initialize_Primal_nodes();
	void start_interface_vectors();
	virtual ~SubDom();
};

#endif /* SUBDOM_H_ */
