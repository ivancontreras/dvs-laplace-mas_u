/*
 * OpInterface.h
 *
 *  Created on: 06/03/2014
 *      Author: argonauta
 */

#ifndef OPINTERFACE_H_
#define OPINTERFACE_H_

#include "./ParallelScheme.hpp"
#include "SubDom.hpp"
#include <iostream>
#include <stdio.h>
#include <math.h>
using namespace std;

#define LEFT   0
#define RIGHT  1
#define DOWN   2
#define UP     3
#define BACK   4
#define FRONT  5

class OpInterface {
public:

	ParallelScheme *P;        //Reference to class that manages MPI.
	SubDom *Sub;
	double *arr_send;         // arrays with the nodes to be exchanged between subdomains.
	double *arr_receive;
	int tot_source;
	int tot_dest;
	OpInterface(ParallelScheme *_p, SubDom *_sub);

	void build_array_values_a( int _source, int _dest);
	void build_array_values_a_prime( int _source, int _dest);
	void clean_array_values();
	void calc_a( double *u_);
	void calc_a_vect( int _source , double *u_); // pointer to primal or dual nodes.
	void calc_a_prime(double *u_);
	void calc_a_prime_vect( int _source , double *u_); // pointer to primal or dual nodes.
	double call_dot(double val);
	double call_norm_infty(double val);
	void exchange_vector( int _source, int _dest);
	void receive_vect_a( int _source , double *u_);
	void send_vect_a( int _dest , double *u_);
	void receive_vect_a_prime( int _source , double *u_);
	void send_vect_a_prime( int _dest , double *u_);

	virtual ~OpInterface();
};

#endif /* OPINTERFACE_H_ */
