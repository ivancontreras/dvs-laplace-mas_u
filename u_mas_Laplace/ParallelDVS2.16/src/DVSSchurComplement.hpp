/*
 * DVSSchurComplement.h
 *
 *  Created on: 31/03/2014
 *      Author: argonauta
 */

#ifndef DVSSCHURCOMPLEMENT_H_
#define DVSSCHURCOMPLEMENT_H_

#include "AlgorithmDVS.hpp"
#include "OpInterface.hpp"


class DVSSchurComplement: public AlgorithmDVS {
public:

	DVSSchurComplement(OpInterface *_op);
	void ApplyOp(double *x, double *y);
	double dot(double *x, double *y);
	void rhs();
	void solve();
	int getSize(void);
	virtual ~DVSSchurComplement();
};

#endif /* DVSSCHURCOMPLEMENT_H_ */
