/*
 * Lineal.h
 *
 *  Created on: 25/09/2010
 *      Author: argonauta
 */

#ifndef LINEAL_H_
#define LINEAL_H_

#include "Func_Tramo.h"

class Lineal : public Func_Tramo {
public:
	Lineal();
	virtual ~Lineal();
	double evalua_funcion(double *x);
	double evalua_derivada(double *x);
	double evalua_segunda_derivada(double *x);

};

#endif /* LINEAL_H_ */
