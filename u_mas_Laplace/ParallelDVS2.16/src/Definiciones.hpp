#ifndef __DEFINICIONES_HPP__
#define __DEFINICIONES_HPP__



/// Activada para trabajar con n�meros double en caso contrario trabajar con long double
#define __Double__

#ifdef __Double__
/// Define ldouble como double
typedef double ldouble;
#else
/// Define ldouble como long double
typedef long double ldouble;
#endif


/// Numero maximo de iteraiones en los metodos iterativos
#define NMAXITER 200


/// Tolerancia en los metodos iterativos
#define EPSILON  1e-7


// Se toman como iguales dos nodos que difieran en menos que esta EPS_EQUAL
#define EPS_EQUAL 1e-15


// Con esta opcion visualiza o no el residual de cada iteracion
#define RESIDUAL

#define NDVECT 3
// Con esta opcion se calcula el numero de condicionamiento en los metodos precondicionados
//#define NUMERO_CONDICIONAMIENTO






// Activacion de las diferentes definiciones para cada problema
#if defined COEF_VARAIBLES_ESTABILIZA
#define ESTABILIZA
#undef  COEFICIENTES_CONSTANTES
#define  OPTIMIZA_RAM

#elif defined COEF_CONSTANTES_ESTABILIZA
#define ESTABILIZA
#undef COEFICIENTES_CONSTANTES
#undef  OPTIMIZA_RAM

#else
#define COEFICIENTES_CONSTANTES
#undef  ESTABILIZA
#undef  OPTIMIZA_RAM

#endif






#endif
