/*
 * DVS_S_inv.h
 *
 *  Created on: 29/05/2014
 *      Author: argonauta
 */

#ifndef DVS_S_INV_H_
#define DVS_S_INV_H_

#include "AlgorithmDVS.hpp"
#include "OpInterface.hpp"

class DVS_S_inv : public AlgorithmDVS{
public:
	DVS_S_inv(OpInterface *_op);
	void ApplyOp(double *x, double *y);
	double dot(double *x, double *y);
	void rhs();
	void solve();
	int getSize(void);
	virtual ~DVS_S_inv();
};

#endif /* DVS_S_INV_H_ */
