/*
 * CombinaCoef.h
 *
 *  Created on: 16/10/2010
 *      Author: argonauta
 */

#ifndef COMBINACOEF_H_
#define COMBINACOEF_H_

class CombinaCoef {
public:
	CombinaCoef(int num_var,int num_val);
	virtual ~CombinaCoef();
	void inicia_valores(int num_var,int num_val);
	bool siguiente();
	bool debo_cambiar(int i);
	bool valor_booleano(int ai_bit);
	int  valor_entero(int ai_bit);
	int  valor_entero_bis(int ai_bit);

    int *z;
    int a,num, num_var;
};

#endif /* COMBINACOEF_H_ */
