/*
 * SchurComplement.h
 *
 *  Created on: 20/03/2014
 *      Author: argonauta
 */

#ifndef SCHURCOMPLEMENT_H_
#define SCHURCOMPLEMENT_H_

#include "OpMatxVect.hpp"
#include "OpInterface.hpp"
#include "IterativeMethods.hpp"
#include <math.h>



class SchurComplement: public OpMatxVect, public IterativeMethods {

public:

    OpInterface *Op;
    bool ext;
    int *map_intA;
    int *map_intV;
    int dim_intA;
    int dim_intV;
	SchurComplement(OpInterface *_op,int dim, int stencil,int *ntp, bool sc_ext);
	void ApplyOp(double *x, double *y);
	void apply_internal_SC(double *w,double *v);
	double dot(double *x, double *y);
	int getSize(void);
	void rhs_SC(double *v_1, int *mapS,double *x);
	void set_parameters_schur_int(int *mA,int dA, int *mV, int dV);
	double *solve_substitution(double *v,double *v1, int *mA, int dA, int *mV, int dV, int *mapS);

	virtual ~SchurComplement();
};

#endif /* SCHURCOMPLEMENT_H_ */
