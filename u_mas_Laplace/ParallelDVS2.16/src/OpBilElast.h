/*
 * OpBilElast.h
 *
 *  Created on: 28/10/2010
 *      Author: argonauta
 */

#ifndef OPBILELAST_H_
#define OPBILELAST_H_

#include "Forma_Bilineal.h"

class OpBilElast: public Forma_Bilineal {
public:
	OpBilElast(int n);
	virtual ~OpBilElast();
	double evalua(double *x);
	void _asigna_funciones(Func_Base *phi,Func_Base *psi);
	void _asigna_derivadas(int i, int j);
	double _grad_div(double *coord);
	double _div_grad(double *coord);
	double lambda,mu;
	int i_,j_,comp;

};

#endif /* OPBILELAST_H_ */
