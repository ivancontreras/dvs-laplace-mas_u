/*
 * DVS_S_inv.cpp
 *
 *  Created on: 29/05/2014
 *      Author: argonauta
 */

#include "DVS_S_inv.hpp"
#include "CGM.hpp"
#include "IterativeMethods.hpp"

DVS_S_inv::DVS_S_inv(OpInterface *_op) : AlgorithmDVS(_op) {
	// TODO Auto-generated constructor stub

}

void DVS_S_inv::ApplyOp(double *x, double *y){

	Apply_S_inv(x, y);
    apply_j(x);

/*	if(Op->Sub->id_sub == -11)
	{
	   cout << "**********************************************" << endl;
		for (int i = 0; i < Op->Sub->n_nodes; i++)
		 if(Op->Sub->mapDual[i]>=0)
		  for(int k=0;k<SchurComp->nDVect;k++)
			cout << "Dual "  << i << " " << Op->Sub->mapDual[i] << " " << y[ Op->Sub->mapDual[i]*SchurComp->nDVect + k ] << endl;
				}*/



}

int DVS_S_inv::getSize(void){

	int val_ret = 0;

	val_ret = SchurComp->dimA;

	return val_ret;
}

double DVS_S_inv::dot(double *x, double *y){

 double dot_prod;
 double ret_dot;

 ret_dot = 0.;

 dot_prod = SchurComp->dot(x,y);


 ret_dot =  Op->call_dot(dot_prod);

 //cout<< Op->Sub->id_sub << " " << dot_prod <<  " "<< ret_dot <<  endl;

 return ret_dot;
}

void DVS_S_inv::rhs(){


	double *f;

	f = new double[SchurComp->dimA*SchurComp->nDVect];
	SchurComp->rhs_SC(0,0,f);
    //<-- f_delta
	for (int i = 0; i < SchurComp->dimA*SchurComp->nDVect; i++)
		uf[i] = f[i];

    Apply_S_inv(rhss,f);
	apply_j(rhss);

}


void DVS_S_inv::solve(){

    double norm_err;
    double norm_err_gl;

	//Define subsets of nodes to be used in the Schur complement.
	SchurComp->set_param_apply(Op->Sub->n_nodes,Op->Sub->mapDual,Op->Sub->nDual,Op->Sub->mapIntPri,Op->Sub->nIntPri,Op->Sub->Fem->FEM->nodo_sub);
	SchurComp->set_parameters_schur_int(Op->Sub->mapPrimal,Op->Sub->nPrim,Op->Sub->mapInt,Op->Sub->nInt);

	//Start arrays to be used in the iterative process.
    rhss = new double[SchurComp->dimA*SchurComp->nDVect];
    u = new double[SchurComp->dimA*SchurComp->nDVect];
    uf = new double[SchurComp->dimA*SchurComp->nDVect];

    // Build right hand side for this Schur Complement.
	rhs();

	IterativeMethods *pt = (IterativeMethods *) this;

	CG = new CGM(*pt, 1e-7);

	CG->rank = Op->P->rank;

	CG->imprime = true;

	CG->solve(u, rhss);

	for (int i = 0; i < SchurComp->dimA*SchurComp->nDVect; i++)
			uf[i] -= u[i];

	Apply_S_inv(u, uf);

	 //if(Op->Sub->id_sub == 13)
/*	   for (int i = 0; i < Op->Sub->n_nodes; i++)
		 if(Op->Sub->mapDual[i]>=0)
		   for(int k=0;k<SchurComp->nDVect;k++)
		      cout << "Dual "  << i << " " << Op->Sub->mapDual[i] << " " << u[ Op->Sub->mapDual[i]*SchurComp->nDVect + k ] << endl;*/

	Op->Sub->print_vector(Op->Sub->DUAL,u);


	//norm_err = calc_err(u);
    //norm_err_gl =  Op->call_dot(norm_err);



	 //after finishing iterative procedure make substitution
}


DVS_S_inv::~DVS_S_inv() {
	// TODO Auto-generated destructor stub
}
