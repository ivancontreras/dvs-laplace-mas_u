/*
 * f_om_elast1.h
 *
 *  Created on: 27/12/2010
 *      Author: argonauta
 */

#ifndef F_OM_ELAST1_H_
#define F_OM_ELAST1_H_

#include "Funcion.h"

class f_om_elast1 : public Funcion{
public:
	f_om_elast1(double lambda, double mu);
	virtual ~f_om_elast1();
	double evalua(double *x);
	double lambda,mu;
};

#endif /* F_OM_ELAST1_H_ */
