/*
 * Func_Tramo.cpp
 *
 *  Created on: 25/09/2010
 *      Author: argonauta
 *      Clase base que especif�ca como se evaluan las funciones de acuerdo con la
 *      direcci�n del elemento , el grado de la derivada que se requiera y por las propias
 *      coordenadas del nodo.
 */

#include "Func_Tramo.h"
#include <iostream>

Func_Tramo::Func_Tramo() {
	// TODO Auto-generated constructor stub

}

Func_Tramo::~Func_Tramo() {
	// TODO Auto-generated destructor stub
}

void Func_Tramo::establece_limites(double izq, double der)
{
   	  lim_der = der;
	  lim_izq = izq;

	}

void Func_Tramo::establece_orden(int ord)
{
   	  orden = ord;

	}

void Func_Tramo::establece_direccion(int dir)
{
   	  direccion = dir;


	}

double Func_Tramo::evalua(double *x)
{

    	switch(orden){
    	case 0 : return evalua_funcion(x);
    	         break;
    	case 1 : return evalua_derivada(x);
    	    	 break;
    	case 2 : return evalua_segunda_derivada(x);
    	    	 break;
    	}


 return *x;

}




