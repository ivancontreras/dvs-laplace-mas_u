/*
 * Func_Base.cpp
 *
 *  Created on: 23/10/2010
 *      Author: argonauta
 */

#include <iostream>
#include "Func_Base.h"
#include "Func_Tramo.h"

Func_Base::Func_Base(Func_Tramo *f_p,int dim) {
	// TODO Auto-generated constructor stub
   int i;

   f = f_p;
   this->dim = dim;
   orden = new int[dim];

   for(i=0;i<dim;i++)
	   orden[i] = 0;

}

Func_Base::~Func_Base() {
	// TODO Auto-generated destructor stub

}

void Func_Base::establece_limites(double **lim)
{

   	  limites = lim;


}

void Func_Base::establece_orden(int *ord)
{
   	  orden = ord;

	}

void Func_Base::establece_direccion(int *dir)
{
   	  direccion = dir;


	}


double  Func_Base::evalua(double *x)
{
	//Modela la operación $\psi_\alpha(\underline{x}) = \Pi \phi(\underline{x})$


	double val_eval;
	int i,k;

	val_eval = 1;


	for(i=0;i<dim;i++)
	 {
		//Establece los valores necesarios
		f->establece_direccion(direccion[i]);
		f->establece_orden(orden[i]);
		f->establece_limites(this->limites[i][0],this->limites[i][1]);
		val_eval *= f->evalua(&x[i]);
	 }

	return val_eval;

 }


