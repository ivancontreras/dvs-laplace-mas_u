/*
 * Funcion.h
 *
 *  Created on: 28/10/2010
 *      Author: argonauta
 *      Clase base (abstracta) de la que heredan todas las funciones Rn->Rm.
 *      En general recibe como parámetros las coordenadas de la variable a evaluar.
 */

#ifndef FUNCION_H_
#define FUNCION_H_

class Funcion {
public:
	Funcion();
	virtual ~Funcion();
	virtual double evalua(double *x)=0;

};

#endif /* FUNCION_H_ */
