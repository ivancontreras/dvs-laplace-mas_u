/*
 * SubDom.cpp
 *
 *  Created on: 21/02/2014
 *      Author: argonauta
 */

#include "SubDom.hpp"
#include "SinPixSinPiySinPiz.h"

SubDom::SubDom(int id, int nDim, int *mesh) : Mesh(id,nDim,mesh){
	// TODO Auto-generated constructor stub

	int i,m;
    int num_nodes, tot_;
    double d;

	this->nDim = nDim;
	this->mesh = mesh;
	id_sub = id;
	nDVect = 1;

	num_nodes = pow(2,nDim);

	nodes_subdomains = new int *[num_nodes];

	for(i=0;i<num_nodes;i++)
		nodes_subdomains[i] = new int[num_nodes-1];

    subdomain = new  double *[nDim];

    for (i = 0; i < nDim; i++)
	  subdomain[i] = new double[2];

	for (i = 0; i < nDim; i++)
	  for (int j = 0; j < 2; j++) subdomain[i][j] = 0.0;

	for (i = 0; i < nDim; i++) M1[i] = 0;
	   h = new double[nDim];

	n_nodes = 1;

	for (i = 0; i < nDim; i++)
	  {
	      N[i] = (i == 0 ? mesh[i] : mesh[i - 1]*N[i - 1]); //coarse mesh
	      M[i] = mesh[i + 3];
	      M1[i] = (i == 0 ? M[i] + 1 : M1[i - 1]*(M[i] + 1)); // fine mesh
	      n_nodes *= (M[i] + 1);
	      M2[i] = (mesh[i] * mesh[i + 3]) + 1;

	   }

	//initialize face vs nodes

	node_face = new int* [nDim*2];

	tot_=1;

	for(i=0;i<(nDim-1);i++)
        tot_*= (mesh[3]+1);

	for(i=0;i<nDim*2;i++)
		node_face[i] = new int[tot_];

	//cout << "Número de nodos por cara " << tot_<< endl;

	genSubCoord(id, coordN, N);

	 for (i = 0; i < nDim; i++)
	   {
	      d = (1.)/mesh[i];
	      subdomain[i][0] = coordN[i]*d;
	      subdomain[i][1] = subdomain[i][0] + d;
	      h[i] = (subdomain[i][1] - subdomain[i][0])/M[i];
	   }


	//number_node_fine_mesh();

    ntype = new int[n_nodes];
    multiplicity = new int[n_nodes];

	for (i = 0; i < n_nodes; i++)
	{
		ntype[i] = 0;
		multiplicity[i] = 1;
	}

	initialize_Primal_nodes();
    genNtype();
    generate_subsets();
    start_interface_vectors();
    defineFace();



    //display_faces();

    //assign_vector_values(DUAL, u_delta, node_dual);
    //assign_vector_values(PRIMAL, u_pi, node_primal);

    //Matrix - vector operations

   // SchurComp = new SchurComplement(nDim,27,true);

    //Discretization Method



	Fem = new FDMCoef(subdomain,M,nDim,h, false,multiplicity,ntype, id_sub);

}

void SubDom::assign_vector_values(int op, double *u_,int *node_){

	int length;

	length = 0;

	for (int i = 0; i < n_nodes; i++)
  	 {
         if(  (ntype[i] & op)!= 0)
          {
        	getCoord(i,x);
            for(int j=0;j<nDVect;j++)
        	 u_[length*nDVect + j] = (double)id_sub;//func_evaluation(x) ;

  		    length++;
          }
  	 }
}


void SubDom::defineFace(){

	//For each node in the subdomain decide in what face it is. Sum up all nodes in one face.

	k_ = new int[nDim*2];

	for(int i=0;i<nDim*2;i++)
	  k_[i]=0;

	for(int m=0; m<n_nodes;m++)
	{

	 getCoord(m,x);

	 if(x[0] == subdomain[0][0])
	 {
	    node_face[0][k_[0]] = m;
	    k_[0]++;
	   }

	  if(x[0] == subdomain[0][1])
	  {
	    node_face[1][k_[1]] = m ;
	    k_[1]++;
	   }

	  if(x[1] == subdomain[1][0])
		 {
		    node_face[2][k_[2]] = m;
		    k_[2]++;
		 }

	  if(x[1] == subdomain[1][1])
		  {
		    node_face[3][k_[3]] = m ;
		    k_[3]++;
		   }


	 if(nDim>2)
	  {
	    if(x[2] == subdomain[2][0])
	     {
		   node_face[4][k_[4]] = m;
		   k_[4]++;
			 }

	    if(x[2] == subdomain[2][1])
		 {
		  node_face[5][k_[5]] = m ;
		  k_[5]++;
			   }

	  }

	}

}
void SubDom::display_faces(){

	int k=0;

	//if(id_sub == 4)
     for(int k=0; k<nDim*2;k++)
	  {
	   cout << " cara " << k << endl;

	   for(int i=0;i < k_[k] ;i++)
		 if((ntype[node_face[k][i]] & DUAL)!= 0)
		   cout << node_dual[node_face[k][i]] << " " << node_face[k][i] << " "<< endl;

	 }

}

void SubDom::display_face_vect(int op, int _source , double *u_, int *node_){

	/*int n_arr;
	int n_p;

	n_arr = 0;
	n_p = 0;

    for(int i=0; i < k_[_source];i++)
		{
		  n_p =  node_face[_source][i];

		  if((ntype[n_p] & op)!= 0)
		  {
			for(int j=0;j<nDim;j++)
			 cout <<  u_[node_[n_p]*nDVect+j] << " " << arr_receive[n_arr*nDVect+j] << " ";

			cout << endl;

			n_arr++;
		    }

		 }*/

}





void SubDom::generate_subsets(){

	 mapFull = new int[n_nodes];

	 for (int i = 0; i < n_nodes; i++)
	  mapFull[i] = 0;

	 nFull = 0;

	 for (int i = 0; i < n_nodes; i++)
	  {
	   mapFull[i] = ( (ntype[i] & KNOWN) != 0  ||  (ntype[i] & PRIMAL) != 0 ? -1 : nFull++);
	   //if ((ntype[i] & DUAL) != 0) nDual++;
		   }

   //mapping for interior plus primal nodes

		 mapIntPri = new int[n_nodes];

		 for (int i = 0; i < n_nodes; i++)
			 mapIntPri[i] = 0;

		 nIntPri = 0;
		 for (int i = 0; i < n_nodes; i++)
			 mapIntPri[i] = ((ntype[i] & INTERIOR) != 0 || (ntype[i] & PRIMAL) != 0 ? nIntPri++ : -1);




		 //mapping for primal nodes

		 mapPrimal = new int[n_nodes];

		 for (int i = 0; i < n_nodes; i++)
			 mapPrimal[i] = 0;

		 nPrim = 0;

		 for (int i = 0; i < n_nodes; i++)
			 mapPrimal[i] = ( (ntype[i] & PRIMAL) != 0 ? nPrim++ : -1);

		 // map for interior nodes
		 mapInt = new  int[n_nodes];

		 for (int i = 0; i < n_nodes; i++)
			 mapInt[i] = 0;

		 nInt = 0;

		 for (int i = 0; i < n_nodes; i++)
			mapInt[i] = ((ntype[i] & INTERIOR) != 0 ? nInt++ : -1);

		 //mapping for dual nodes

		  mapDual = new int[n_nodes];

		  for (int i = 0; i < n_nodes; i++)
			  mapDual[i] = 0;

		  nDual = 0;

		  for (int i = 0; i < n_nodes; i++)
			  mapDual[i] = ( (ntype[i] & DUAL) != 0 ? nDual++ : -1);

		   // equation mapping for Boundary nodes.
		  mapFullPri = new  int[n_nodes];

		  for (int i = 0; i < n_nodes; i++)
			  mapFullPri[i] = 0;

		  nBdPrim = 0;

		  for (int i = 0; i < n_nodes; i++)
			  mapFullPri[i] = ( (ntype[i] & KNOWN) != 0 ? -1 : nBdPrim++);

		 /*cout << "Total de primales " << nPrim << endl;
		 cout << "Total de interiores " << nInt << endl;
		 cout << "Total de duales " << nDual << endl;*/

}

double SubDom::norma_A(int op, double *u_)
{

	int length;
	double err_abs;
	double err_ret;

         length = 0;
	SinPixSinPiySinPiz *exacta = new SinPixSinPiySinPiz();
    err_ret = 0.;
    err_abs = 0.;

	for (int i = 0; i < n_nodes; i++)
  	 {
         if(  (ntype[i] & op)!= 0)
          {

        	getCoord(i,x);
            for(int j=0;j<nDVect;j++)
             {
        	    err_abs = fabs(u_[length*nDVect + j] - exacta->evalua(x));
        	    if(err_abs > 0.8) cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << err_abs << endl;

        	    if(err_abs > err_ret) err_ret = err_abs;
               }

  		    length++;
          }
  	 }


   delete exacta;

   return err_ret;

}

void SubDom::print_interface(int op, int _source , double *u_, int *node_){

	/*int n_arr;
    int n_p;
    string name;
    ostringstream convert;
    n_arr = 0;
    n_p = 0;

    convert << id_sub;

    //name.assign( "FaceValues.txt");
    name.assign( "FaceValues"+convert.str() + ".txt");
    //name.append(convert.str());
	ofstream myfile;
	myfile.open (name.data());

	 for(int i=0; i < k_[_source];i++)
			{
			  n_p =  node_face[_source][i];

			  if((ntype[n_p] & op)!= 0)
			  {

			    getCoord(n_p,x);

				for(int j=0;j<nDim;j++)
					myfile <<  x[j] + u_[node_[n_p]*nDVect+j] << " ";

				for(int j=0;j<nDim;j++)
					myfile << x[j] + arr_receive[n_arr*nDVect+j] << " ";

				myfile << endl;

				n_arr++;
			    }

			 }

	myfile.close();*/

}


void SubDom::print_vector(int op, double *u_){

	int length;

	SinPixSinPiySinPiz *exacta = new SinPixSinPiySinPiz();



	length = 0;

	for (int i = 0; i < n_nodes; i++)
  	 {
         if(  (ntype[i] & op)!= 0)
          {
        	getCoord(i,x);
            for(int j=0;j<nDVect;j++)
        	 cout << u_[length*nDVect + j] << " analitica " << exacta->evalua(x) << endl;

  		    length++;
          }
  	 }


}


void SubDom::genNtype()
{

	 int m, type = 0, r = 0;

	 x = new double[nDim];

	 for (m = 0; m < n_nodes; m++)
	  {
	   genSubCoord(m, coord, M1);
	   type = 0;

	   getCoord(m,x);


       if (isKnown(coord))
		   type |= KNOWN;
       else
    	 if (isInterior(coord))
		        {
			     type |= INTERIOR;
			     multiplicity[m] = 1;
		        }
		   else
		    {
		      isIntBd(coord);
		      type |= INTBD;
		      r = nodeType(coord);

		      if (r == nDim){
		    	  type |= VERTEX;
		    	  multiplicity[m] = 4;
		      }
		       else
		         if (nDim == 3 && r == 1){
		        	 type |= FACE;
		        	 multiplicity[m] = 2;
		         }
		         else {
		        	 type |= EDGE;
		        	 multiplicity[m] = 2;
		         }
		        }

		       if ((type & INTBD) != 0 && primal->isPrimal(type, coordN, coord))
		           type |= PRIMAL;

		       if ((type & INTBD) != 0 && (type & PRIMAL) == 0)
		    	   type |= DUAL;

		        ntype[m] = type;

	   }

}

void SubDom::initialize_Primal_nodes(){

	char *prim;

	  //prim = "vertedge";

	  //if (strcmp(prim,"vertex") == 0)

	 // primal = new  VertPrimal();

	  primal = new  VertEdgePrimal();

	  /*else if (strcmp(prim,"vertedge") == 0) primal = new  VertEdgePrimal();
	  else if (strcmp(prim,"all") == 0) primal = new  AllPrimal();
	  else primal = new  NoPrimal();

	  if (primal == NULL)
		  ;*/

}

// It defines what kind of node is every node in the subdomain.

int SubDom::nodeType(int *coord)
{
	 int n = 0;

	 for (int i = 0; i < nDim; i++)
	   if (coord[i] == 0 || coord[i] == M[i])
	      n++;

	 return n;

}



void SubDom::start_interface_vectors(){

	l_dual = 0;
	l_primal = 0;

	for (int i = 0; i < n_nodes; i++)
	 {
       if(  (ntype[i] & DUAL)!= 0)
		l_dual++;

       if(  (ntype[i] & PRIMAL)!= 0)
		l_primal++;
	   }

      u_delta = new double[l_dual*nDVect];
      u_pi = new double[l_primal*nDVect];
      node_primal = new int[n_nodes];
      node_dual = new int[n_nodes];

  	l_dual = 0;
  	l_primal = 0;

  	for (int i = 0; i < n_nodes; i++)
  	 {
         if(  (ntype[i] & DUAL)!= 0)
          {
            node_dual[i] = l_dual;
  		    l_dual++;
          }
         else
        	 node_dual[i] = -1;

         if(  (ntype[i] & PRIMAL)!= 0)
         {
           node_primal[i] = l_primal;
  		   l_primal++;
         }
         else
           node_primal[i] = -1;

  	   }
}

SubDom::~SubDom() {
	// TODO Auto-generated destructor stub


}
