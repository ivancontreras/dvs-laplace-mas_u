#ifndef __VertEdgePrimal__
#define __VertEdgePrimal__

#include "Primal.hpp"


class VertEdgePrimal : public Primal
{

public:

   const char *name;

   VertEdgePrimal(void) : name("VertEdgePrimal")
   { }

   inline bool isPrimal(int type, int *coordN, int *coordM)
   {
      //printf("%s",name);
      return ((type & VERTEX) != 0 || (type & EDGE) != 0);
   }

};

#endif
