/*
 * Mesh.cpp
 *
 *  Created on: 05/03/2014
 *      Author: argonauta
 */

#include "Mesh.hpp"

Mesh::Mesh(int id, int nDim, int *mesh) {
	// TODO Auto-generated constructor stub
	int i;

	coordN = new int[3];
		coord = new int[3];

		for (i = 0; i < 3; i++)
		  coordN[i] = 0;

		N = new  int[3];

		for (i = 0; i < 3; i++)
	      N[i] = 0;

		M = new int[3];

		for (i = 0; i < 3; i++)
		  M[i] = 0;

		M1 = new int[3];

		for (i = 0; i < 3; i++)
		  M1[i] = 0;

		M2 = new int[3];

		for (i = 0; i < 3; i++)
		  M2[i] = 0;

}

// Define the number of partion in every coordinate with respect to the subdomain.
void Mesh::genSubCoord(int n, int *coord, int *N)
{

   for (int i = nDim - 1; i >= 0; i--)
   {
      coord[i] = (i == 0 ? n : n / N[i - 1]);

      if (i > 0) n -= coord[i]*N[i - 1];
   }
}



void Mesh::getCoord(int m, double *x)
{

   genSubCoord(m, coord, M1);

   for (int i = 0; i < nDim; i++)
	   x[i] = subdomain[i][0] + coord[i]*(subdomain[i][1] - subdomain[i][0])/(double)M[i];

}

bool Mesh::isKnown(int *coord)
{
	for (int i = 0; i < nDim; i++)
     {
       if ((coord[i] == 0 && coordN[i] == 0) || (coord[i] == M[i] && coordN[i] == mesh[i] - 1))
         return true;

       }

     return false;
}

bool Mesh::isInterior(int *coord)
{
    for (int i = 0; i < nDim; i++)
      if (coord[i] == 0 || coord[i] == M[i]) return false;
         return true;

 return false;

}

bool Mesh::isIntBd(int *coord)
{
	for (int i = 0; i < nDim; i++)
	   if (coord[i] == 0 || coord[i] == M[i])
	      if (coordN[i] != 0 && coordN[i] != N[i])
	    	  return true;

	   return false;
}

Mesh::~Mesh() {
	// TODO Auto-generated destructor stub
}
