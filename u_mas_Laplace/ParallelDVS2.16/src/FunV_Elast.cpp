/*
 * FunV_Elast.cpp
 *
 *  Created on: 27/12/2010
 *      Author: argonauta
 */

#include "FunV_Elast.h"
#include "./f_om_elast1.h"
#include "./f_om_elast2.h"
#include "./f_om_elast3.h"

FunV_Elast::FunV_Elast() {
	// TODO Auto-generated constructor stub

	double lambda,mu;
	double alpha = -1e-3;
	double E_Poisson = 68.95e9;
	double nu = 26e-2;
	double nu1;

	lambda = E_Poisson * nu;
	lambda /= (1+nu)*(1-2*nu);
	mu = E_Poisson ;
	mu /= 2*(1+nu);
	nu1 = lambda;
	nu1 /= 2*(lambda + mu);

	//printf("\n****************Coeficientes de Lam� mu %e lambda %e  ", mu,lambda);

    /*lambda = 1.0;
    mu = 1.0;*/

	f_der[0] = new f_om_elast1(lambda,mu);
	f_der[1] = new f_om_elast2(lambda,mu);
	f_der[2] = new f_om_elast3(lambda,mu);
}

FunV_Elast::~FunV_Elast() {
	// TODO Auto-generated destructor stub
}

double FunV_Elast::evalua(double *x)
{
	double val_ret=0;

	return val_ret;

}
