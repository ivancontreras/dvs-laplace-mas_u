/*
 * f_om_elast2.h
 *
 *  Created on: 27/12/2010
 *      Author: argonauta
 */

#ifndef F_OM_ELAST2_H_
#define F_OM_ELAST2_H_

#include "Funcion.h"

class f_om_elast2  : public Funcion{
public:
	f_om_elast2(double lambda, double mu);
	virtual ~f_om_elast2();
	double evalua(double *x);
	double lambda,mu;
};

#endif /* F_OM_ELAST2_H_ */
