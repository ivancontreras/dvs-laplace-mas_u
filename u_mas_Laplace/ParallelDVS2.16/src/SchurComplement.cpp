/*
 * SchurComplement.cpp
 *
 *  Created on: 20/03/2014
 *      Author: argonauta
 */

#include "SchurComplement.hpp"
#include "CGM.hpp"
#include "IterativeMethods.hpp"

SchurComplement::SchurComplement(OpInterface *_op,int dim, int stencil, int *ntp , bool sc_ext):OpMatxVect(dim, stencil,ntp) {
	// TODO Auto-generated constructor stub
    ext = sc_ext;
    Op = _op;
    // parameters of execution are defiend here.
    //MatxVectValues(_op->Sub->Fem->FEM->coef, _op->Sub->Fem->FEM->rhs);
    //IterativeMethods *pt = (IterativeMethods *) this;
    //CG = new CGM(*pt, 1e-7);

}

void SchurComplement::ApplyOp(double *x, double *y){

	int n_p,face; // eliminar
	double *v;
	double *z;

	v = new double[dimV*nDVect];
	z = new double[dimA*nDVect];


	//Aplica ANM al vector u_M

    if(ext)
      Op->calc_a_prime(y);


	applyMtxVect(v,y,mapV, mapA);


    //Aplicar ANN^1

	if(ext)
	  inverse(v,v);
	else
	  apply_internal_SC(v,v);

    // Apply AMN to vector u_N

	applyMtxVect(x,v,mapA,mapV);


    // Apply AMM to vector u_M

	applyMtxVect(z,y,mapA,mapA);

	if(!ext)
	  Op->calc_a(z);

    // get difference of Apipi upi - ApiIAIIAIpi u_pi

	difference_vectors(z,x,dimA);  // result stays in x.

	if(ext)
      Op->calc_a_prime(x);

	delete []v;
	delete []z;

}


void SchurComplement::apply_internal_SC(double *w,double *v){

	int _d;
    SchurComplement *SC;
    Solvable *CG;

	int i,j,n_p;
    double *X;
	double *Y;
	double *z;


    /*for (int i = 0; i <  dimA*nDVect; i++)
  	  cout << i << " "  << v[i] << endl;*/

	_d = this->Op->Sub->Fem->FEM->tam_arreglo;

	SC = new SchurComplement(Op,nDVect,_d,ntype,true);

    SC->MatxVectValues(coef, rhs);

	SC->set_param_apply(num_nodes,map_intA,dim_intA,map_intV,dim_intV,nodo_sub);

    IterativeMethods *pt = (IterativeMethods *) SC;

	CG = new CGM(*pt, 1e-7);

    X = new double[dim_intA*nDVect];

    Y = new double[dim_intA*nDVect];

	for (i = 0; i < dim_intA*nDVect; i++) X[i] = 0.0;

	SC->inverse_A_NN(map_intV,dim_intV);

	// Define the right hand side for this Schur complement.

	SC->rhs_SC(v,mapV,Y);

	Op->calc_a_prime(Y);



	// Solve first unknown with CGM.

	CG->imprime = false;
	CG->rank = Op->P->rank;
    CG->solve(X, Y);



	//Solve second unknown applying substitution.

    z = SC->solve_substitution(X,v,map_intA,dim_intA,map_intV,dim_intV,mapV);


    for (i = 0; i < num_nodes; i++)
	 if(map_intA[i]>=0)
	  for(int k=0;k<nDVect;k++)
	    w[mapV[i]*nDVect + k] = X[map_intA[i]*nDVect + k];

	for (i = 0; i < num_nodes; i++)
	 if(map_intV[i]>=0)
	  for(int k=0;k<nDVect;k++)
	    w[ mapV[i]*nDVect + k ] = z[map_intV[i]*nDVect + k];


	delete []X;
	delete []Y;


	pt = 0;
	delete pt;
    delete CG;
    delete SC;

}

double SchurComplement::dot(double *u, double *v){

	double value;
	double ret_dot;

	value = 0.;
	ret_dot = 0.;

	for (int i = 0; i < dimA*nDVect; i++)
		value += u[i]*v[i];

	if(ext)
		 ret_dot =  Op->call_dot(value);
	else
	   ret_dot = value;

	return ret_dot;

}

int SchurComplement::getSize(void){

	return dimA;
}

void SchurComplement::rhs_SC(double *v1, int *mapS,double *x){

   int i,j,k;

   double *w;
   double *b_A;
   double *b_V;
   double *b_S;

   b_A = new double[dimA*nDVect];
   b_V = new double[dimV*nDVect];

  if(!ext)
   b_S = new double[dim_intA*nDVect];
  


  w = new double[dimV*nDVect];

   for (i = 0; i < dimV*nDVect; i++) b_V[i] = 0.0;

   //get rhs from original system of linear equations.
   if(v1 == 0)
   {
     recover_rhs(b_A, mapA);
     recover_rhs(b_V, mapV);
     recover_rhs(b_S ,map_intA);
     Op->calc_a(b_A);
     Op->calc_a_prime(b_S);

 	for (i = 0; i < num_nodes; i++)
 	 if(map_intA[i]>=0)
 	  for(int k=0;k<nDVect;k++)
 	    b_V[mapV[i]*nDVect + k] = b_S[map_intA[i]*nDVect + k];



   }
   //get rhs from local application.
   else
   {
	  recover_rhs(b_A, mapA,v1,mapS);
	  recover_rhs(b_V, mapV,v1,mapS);

   }

  /*Apply ANN^1 by solving the system v = A_NN w with a direct or iterative method*/

   if( v1 == 0 )
   {
	 //cout << " Por aquí pasó Colón con su batallón" << endl;


     apply_internal_SC(w,b_V);

	 /*  for(int i = 0; i < this->dimV*nDVect ; i++)
	        		     cout << Op->Sub->id_sub << " "<<w[i] << std::endl;*/
   }
   else
     inverse(w,b_V);

   if(Op->Sub->id_sub == -13 && !ext )
      for(int i = 0; i < this->dimA*nDVect ; i++)
  		     cout << Op->Sub->id_sub << " "<<w[i] << std::endl;



    //Apply A_MN to vector u_N
    applyMtxVect(x,w,mapA,mapV);

   /* for(int i = 0; i < this->dimA*nDVect ; i++)
     		     cout << Op->Sub->id_sub << " "<<x[i] << std::endl;*/

  if(!ext)
     Op->calc_a(x);


    //Difference  f_S - ASCI(ACC^-1) u_C
    difference_vectors(b_A,x,dimA);// f_delta is stored in vector *x.




   delete []b_A;
   delete []w;

   if(!ext)
    delete []b_S;


}

void SchurComplement::set_parameters_schur_int(int *mA,int dA, int *mV, int dV){

	map_intA = mA;
	map_intV = mV;
	dim_intA = dA;
	dim_intV = dV;
}

double * SchurComplement::solve_substitution(double *v,double *v1, int *mA, int dA, int *mV, int dV, int *mapS){

	int i,k;

	double *y;
	double *b_A;

	b_A = new double[dV*nDVect];
	y = new double[dV*nDVect];


	for(i=0;i<dV*nDVect;i++)
		y[i] = 0.;
	// Solve the second variable by substitution.

	if(v1==0)
	  recover_rhs(b_A, mV);
	else
	  recover_rhs(b_A, mapV,v1,mapS);


	//AII^1 u_I = f-AIpiu_pi that is the same as u_I = AII (f-AIpiu_pi)
	applyMtxVect(y,v,mV, mA);

	difference_vectors(b_A,y,dV);

    if(v1 == 0 )
	    apply_internal_SC(y,y);
	else
		inverse(y,y);

	return y;
}

SchurComplement::~SchurComplement() {
	// TODO Auto-generated destructor stub

}
