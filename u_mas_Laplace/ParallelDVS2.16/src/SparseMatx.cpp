/*
 * SparseMatx.cpp
 *
 *  Created on: 19/03/2014
 *      Author: argonauta
 */

#include "SparseMatx.hpp"

SparseMatx::SparseMatx( int ren,  int col, int ban) {
	// TODO Auto-generated constructor stub
	 alloc_memory(ren, col, ban);

}

void SparseMatx::alloc_memory(int ren,  int col,  int ban)
{
   int i;

   Ren = ren;
   Col = col;
   Ban = ban;// > col? col : ban;

   M = new double *[Ren];

   for (i = 0; i < Ren; i++)
	 M[i] = 0;

   M[0] = new double[Ban];

   for (i = 0; i < Ban; i++)
	  M[0][i] = 0.0;


   J = new int *[Ren];

   for (i = 0; i < Ren; i++)
	  J[i] = 0;

   J[0] = new  int[Ban];

   for (i = 0; i < Ban; i++)
	   J[0][i] = -1;
}



void SparseMatx::alloc( int ren, int col, double val)
{

   int i, k;

   // alloc memory for rows
   if (M[ren] == 0)
   {
      M[ren] = new  double[Ban];

      J[ren] = new  int[Ban];

      for (i = 0; i < Ban; i++)
      {
         M[ren][i] = 0.0;
         J[ren][i] = -1;
      }
   }

   // set the band to zero
   if (val == 0.0)
   {
      // if there are values then eliminate them
      k = 0;
      while (k < Ban)
      {
         if (J[ren][k] == -1) break;
         // column founded
         if (J[ren][k] == col)
         {
            // reorder values in the band
            for (i = k+1; i < Ban; i++)
            {
               M[ren][i-1] = M[ren][i];
               J[ren][i-1] = J[ren][i];
            }
            M[ren][Ban-1] = 0.0;
            J[ren][Ban-1] = -1;
            return;
         }
         // Look for the next column
         k++;
      }
   }
   else
   {
      // set value
      k = 0;
      while (J[ren][k] != -1 && k < Ban)
      {
         // column founded
         if (J[ren][k] == col)
         {
            // change value
            M[ren][k] = val;
            return;
         }
         // Look for the next column
         k++;

      }
      // set the value in the first free place.
      M[ren][k] = val;
      J[ren][k] = col;
   }
}

int SparseMatx::num_col_ban(int ren)
{


   if (M[ren] == 0) return 0;
   int k = 0;

   while (k < Ban)
   {
      if (J[ren][k] == -1) break;
      k++;
   }
   return k;
}

SparseMatx::~SparseMatx()
{
   int i;

   if (M)
   {
      for (i = 0; i < Ren; i++)
      {
         if (M[i])
         {
            delete []M[i];
            M[i] = 0;
            delete []J[i];
            J[i] = 0;
         }
      }
      delete []M;
      M = 0;
   }

   delete []J;
   J = 0;
}
