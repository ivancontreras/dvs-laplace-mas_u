/*
 * BilinealGrad.h
 *
 *  Created on: 12/11/2010
 *      Author: argonauta
 */

#ifndef BILINEALGRAD_H_
#define BILINEALGRAD_H_
#include "Forma_Bilineal.h"

class BilinealGrad : public Forma_Bilineal{
public:
	BilinealGrad(int n);
	virtual ~BilinealGrad();
	double evalua(double *x);
	void _asigna_funciones(Func_Base *phi,Func_Base *psi);



};

#endif /* BILINEALGRAD_H_ */
