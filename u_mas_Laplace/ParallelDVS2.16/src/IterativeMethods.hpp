/*
 * IterativeMethods.h
 *
 *  Created on: 25/03/2014
 *      Author: argonauta
 */

#ifndef ITERATIVEMETHODS_H_
#define ITERATIVEMETHODS_H_

class IterativeMethods {
public:

	virtual void ApplyOp(double *x, double *y)=0;
	virtual double dot(double *x, double *y)=0;
	virtual int getSize(void)=0;
};

#endif /* ITERATIVEMETHODS_H_ */
