/*
 * Funcional_rhs.h
 *
 *  Created on: 21/11/2010
 *      Author: argonauta
 */

#ifndef FUNCIONAL_RHS_H_
#define FUNCIONAL_RHS_H_
#include "Forma_Bilineal.h"

class Funcional_rhs : public Forma_Bilineal{
public:
	Funcional_rhs(int n);
	virtual ~Funcional_rhs();
	double evalua(double *x);
	void _asigna_funciones(Funcion *f,Func_Base *psi);

};

#endif /* FUNCIONAL_RHS_H_ */
