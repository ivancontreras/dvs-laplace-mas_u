/*
 * Mesh.h
 *
 *  Created on: 05/03/2014
 *      Author: argonauta
 */

#ifndef MESH_H_
#define MESH_H_

class Mesh {

protected:

    int *N;
   int *M;                    // mesh size [number of subdivisions per axis].
   int *M1;
   int *M2;                   // defines partitions on every axis.
   int *mesh;
   double *h;                 // mesh size for each dimension
   double **subdomain;          // [nDim][2]  min, max in each dimension.
   int *coordN;                // integer coordinates of node number ()
   int *coord;                 // integer coordinates of node number (fine mesh)

   void genSubCoord(int n, int *coord, int *N);
   bool isKnown(int *coord);
   bool isInterior(int *coord);
   bool isIntBd(int *coord);
public:
    int nDim;
    static const int KNOWN = 1, INTERIOR = 2, INTBD = 4, VERTEX = 8, EDGE = 16, FACE = 32, PRIMAL = 64, DUAL = 128;
	Mesh(int id, int nDim, int *mesh);
	void getCoord(int m, double *x);
	virtual ~Mesh();
};

#endif /* MESH_H_ */
